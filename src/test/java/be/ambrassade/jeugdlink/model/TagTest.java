package be.ambrassade.jeugdlink.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TagTest {
    private Tag tag;

    private String[] excludedProperties = {"questions", "users"};

    @BeforeEach
    void setUp() {
        tag = new Tag();
    }

    @Test
    void testNoArgConstructor() {
        assertThat(Tag.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(Tag.class, hasValidGettersAndSetters());
    }

    @Disabled("Disabled because the hasCode() is removed due to stackoverflow bug")
    @Test
    void hasValidBeanHashCodeMethod() {
        assertThat(Tag.class, hasValidBeanHashCodeExcluding(excludedProperties));
    }

    @Test
    void hasValidEqualsMethod() {
        assertThat(Tag.class, hasValidBeanEqualsExcluding(excludedProperties));
    }

    @Test
    void areTheQuestionsProperlyReturned() {
        Set<Question> questionSet = new HashSet<>();
        Question question = new Question("test", "dit is een test", new User());
        Question question1 = new Question("test2", "nog eentje", new User());
        questionSet.add(question);
        questionSet.add(question1);
        tag.setQuestions(questionSet);

        assertEquals(questionSet, tag.getQuestions());
    }

    @Test
    void areTheUsersProperlyReturned() {
        Set<User> users = new HashSet<>();
        User user = new User("ik", "test@test.com", "test123");
        User user1 = new User("jos", "jos@go.be", "test1234");
        users.add(user);
        users.add(user1);
        tag.setUsers(users);

        assertEquals(users, tag.getUsers());
    }
}