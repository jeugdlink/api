package be.ambrassade.jeugdlink.model;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class MessageTest {
    @Test
    void testNoArgConstructor() {
        assertThat(Message.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(Message.class, hasValidGettersAndSetters());
    }

    @Disabled("Disabled because the hasCode() is removed due to stackoverflow bug")
    @Test
    void hasValidBeanHashCodeMethod() {
        assertThat(Message.class, hasValidBeanHashCode());
    }

    @Test
    void hasValidEqualsMethod() {
        assertThat(Message.class, hasValidBeanEquals());
    }
}
