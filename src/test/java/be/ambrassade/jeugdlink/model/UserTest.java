/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.SecureRandom;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserTest {
    private User user;

    private String[] excludedProperties = {
            "accountNonExpired",
            "accountNonLocked",
            "authorities",
            "credentialsNonExpired",
            "enabled",
            "registeredOn",
            "username",
            "password"
    };

    @BeforeEach
    void setUp() {
        user = new User();
    }

    @Test
    void testNoArgConstructor() {
        assertThat(User.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(User.class, hasValidGettersAndSettersExcluding(excludedProperties));
    }

    @Disabled("Disabled because the hasCode() is removed due to stackoverflow bug")
    @Test
    void hasValidBeanHashCodeMethod() {
        assertThat(User.class, hasValidBeanHashCodeExcluding(excludedProperties));
    }

    @Test
    void hasValidEqualsMethod() {
        assertThat(User.class, hasValidBeanEqualsExcluding(excludedProperties));
    }

    @Test
    void doesGetUsernameReturnEmailCorrectly() {
        user.setEmail("ik@get.com");

        assertEquals(user.getUsername(), user.getEmail());
    }

    @Test
    void doesBcryptPasswordEncodingWork() {
        SecureRandom random = new SecureRandom();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String psw = "This is my password.";
        String psw2 = "54df54dfd 84!!è!fg fdg54er5r4tr48yt45yè!èà!çà!ç898 §(§  ,;,;,;,;";

        user.setPassword(psw);
        assertTrue(encoder.matches(psw, user.getPassword()));

        user.setPassword(psw2);
        assertTrue(encoder.matches(psw2, user.getPassword()));
    }

    @Test
    void organisationShouldBeCorrectlyReturned() {
        Organisation organisation = new Organisation();
        organisation.setName("get");
        user.setOrganisation(organisation);

        assertEquals("get", user.getOrganisation().getName());
    }
}