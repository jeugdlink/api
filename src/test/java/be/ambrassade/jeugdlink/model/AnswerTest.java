package be.ambrassade.jeugdlink.model;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

class AnswerTest {
    @Test
    void testNoArgConstructor() {
        assertThat(Answer.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(Answer.class, hasValidGettersAndSetters());
    }

    @Disabled("Disabled because the hasCode() is removed due to stackoverflow bug")
    @Test
    void hasValidBeanHashCodeMethod() {
        assertThat(Answer.class, hasValidBeanHashCode());
    }

    @Test
    void hasValidEqualsMethod() {
        assertThat(Answer.class, hasValidBeanEquals());
    }
}
