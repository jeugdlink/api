package be.ambrassade.jeugdlink.model.response;

import org.junit.jupiter.api.Test;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;

class AuthenticatedProfileResponseTest {
    @Test
    void testNoArgConstructor() {
        assertThat(AuthenticatedProfileResponseTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(AuthenticatedProfileResponseTest.class, hasValidGettersAndSetters());
    }

}