package be.ambrassade.jeugdlink.model.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class QuestionRequestTest {
    private Validator validator;
    private QuestionRequest request;

    @BeforeEach
    void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        request = new QuestionRequest();
        request.setMessage("test");
        request.setQuestion("questionstring");
        request.setAnswered(false);
        request.setTags(new String[]{"test"});
    }

    @Test
    void testNoArgConstructor() {
        assertThat(QuestionRequestTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(QuestionRequestTest.class, hasValidGettersAndSetters());
    }

    @Test
    void messageTooLongReturnsValidationError() {
        request.setMessage(new String(new char[1001]).replace("\0", " "));
        Set<ConstraintViolation<QuestionRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void questionIsNullReturnsValidationError() {
        request.setQuestion(null);
        Set<ConstraintViolation<QuestionRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void questionSizeTooShortOrLongReturnsValidationError() {
        request.setQuestion("hoi");
        Set<ConstraintViolation<QuestionRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations.iterator().next().getMessageTemplate());

        request.setQuestion(new String(new char[256]).replace("\0", " "));
        Set<ConstraintViolation<QuestionRequest>> violations1 = validator.validate(request);

        assertEquals(1, violations1.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations1.iterator().next().getMessageTemplate());
    }
}
