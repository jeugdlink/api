package be.ambrassade.jeugdlink.model.request;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;

@Disabled
class UserEditRequestTest {
    @Test
    void testNoArgConstructor() {
        assertThat(UserEditRequestTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(UserEditRequestTest.class, hasValidGettersAndSetters());
    }
}
