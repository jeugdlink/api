package be.ambrassade.jeugdlink.model.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AnswerRequestTest {
    private Validator validator;
    private AnswerRequest request;

    @BeforeEach
    void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        request = new AnswerRequest();
    }

    @Test
    void testNoArgConstructor() {
        assertThat(AnswerRequestTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(AnswerRequestTest.class, hasValidGettersAndSetters());
    }

    @Test
    void messageIsNullReturnsValidationError() {
        Set<ConstraintViolation<AnswerRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void messageSizeTooShortOrLongReturnsValidationError() {
        request.setMessage("test");
        Set<ConstraintViolation<AnswerRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations.iterator().next().getMessageTemplate());

        request.setMessage(new String(new char[1001]).replace("\0", " "));
        Set<ConstraintViolation<AnswerRequest>> violations1 = validator.validate(request);

        assertEquals(1, violations1.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations1.iterator().next().getMessageTemplate());
    }
}
