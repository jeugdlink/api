package be.ambrassade.jeugdlink.model.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ActionRequestTest {
    private Validator validator;
    private ActionRequest request;

    @BeforeEach
    void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        request = new ActionRequest();
        request.setValue("test");
        request.setAction("actiontest");
    }

    @Test
    void testNoArgConstructor() {
        assertThat(ActionRequestTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(ActionRequestTest.class, hasValidGettersAndSetters());
    }

    @Test
    void actionIsNullReturnsValidationError() {
        request.setAction(null);
        Set<ConstraintViolation<ActionRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void valueIsNullReturnsValidationError() {
        request.setValue(null);
        Set<ConstraintViolation<ActionRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                violations.iterator().next().getMessageTemplate());
    }
}
