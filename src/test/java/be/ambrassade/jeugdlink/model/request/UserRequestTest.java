package be.ambrassade.jeugdlink.model.request;

import org.junit.jupiter.api.Test;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;

class UserRequestTest {
    @Test
    void testNoArgConstructor() {
        assertThat(UserRequestTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(UserRequestTest.class, hasValidGettersAndSetters());
    }
}
