package be.ambrassade.jeugdlink.model.request;

import be.ambrassade.jeugdlink.repo.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
class UserCreateRequestTest {
    private Validator validator;

    @InjectMocks
    private UserCreateRequest request;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        request = new UserCreateRequest();
        request.setEmail("not@me.com");
        request.setPassword("123456789123");

        Mockito.when(userRepository.existsByEmail(Mockito.anyString())).thenReturn(true);

    }

    @Test
    void testNoArgConstructor() {
        assertThat(UserCreateRequestTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(UserCreateRequestTest.class, hasValidGettersAndSetters());
    }

    @Test
    void emailIsNullReturnsValidationError() {
        request.setEmail(null);
        Set<ConstraintViolation<UserCreateRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void passwordIsNullReturnsValidationError() {
        request.setPassword(null);
        Set<ConstraintViolation<UserCreateRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                violations.iterator().next().getMessageTemplate());

    }

    @Test
    void passwordSizeTooShortOrLongReturnsValidationError() {
        request.setPassword("lol");
        Set<ConstraintViolation<UserCreateRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations.iterator().next().getMessageTemplate());

        request.setPassword(new String(new char[85]).replace("\0", " "));
        Set<ConstraintViolation<UserCreateRequest>> violations1 = validator.validate(request);

        assertEquals(1, violations1.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations1.iterator().next().getMessageTemplate());
    }

    @Test
    void emailNotEmailFormatReturnsValidationError() {
        request.setEmail("fakemail");
        Set<ConstraintViolation<UserCreateRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.Email.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void emailNotUniqueReturnsValidationError() {
        Mockito.when(userRepository.existsByEmail("ik@lol.be")).thenReturn(true);
        Mockito.when(userRepository.existsByEmail("ikniet@lol.be")).thenReturn(false);

        request.setEmail("ik@lol.be");
        Set<ConstraintViolation<UserCreateRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("mag niet al geassocieerd zijn met een account",
                violations.iterator().next().getMessageTemplate());

        request.setEmail("ikniet@lol.be");
        Set<ConstraintViolation<UserCreateRequest>> violations1 = validator.validate(request);

        assertEquals(0, violations1.size());
    }
}
