package be.ambrassade.jeugdlink.model.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageRequestTest {
    private Validator validator;
    private MessageRequest request;

    @BeforeEach
    void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        request = new MessageRequest();
        request.setMessage("test");
        request.setRecipient(1);
        request.setSubject("subject");
    }

    @Test
    void testNoArgConstructor() {
        assertThat(MessageRequestTest.class, hasValidBeanConstructor());
    }

    @Test
    void gettersAndSettersShouldWorkForEachProperty() {
        assertThat(MessageRequestTest.class, hasValidGettersAndSetters());
    }

    @Test
    void recipientIsNullReturnsValidationError() {
        request.setRecipient(null);
        Set<ConstraintViolation<MessageRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.NotNull.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void subjectIsNullIsAllowed() {
        request.setSubject(null);
        Set<ConstraintViolation<MessageRequest>> violations = validator.validate(request);

        assertEquals(0, violations.size());
    }

    @Test
    void subjectSizeTooLongReturnsValidationError() {
        request.setSubject(new String(new char[101]).replace("\0", " "));
        Set<ConstraintViolation<MessageRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations.iterator().next().getMessageTemplate());
    }

    @Test
    void messageSizeTooShortOrLongReturnsValidationError() {
        request.setMessage("");
        Set<ConstraintViolation<MessageRequest>> violations = validator.validate(request);

        assertEquals(1, violations.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations.iterator().next().getMessageTemplate());

        request.setMessage(new String(new char[1001]).replace("\0", " "));
        Set<ConstraintViolation<MessageRequest>> violations1 = validator.validate(request);

        assertEquals(1, violations1.size());
        assertEquals("{javax.validation.constraints.Size.message}",
                violations1.iterator().next().getMessageTemplate());
    }
}
