/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.controller;

import be.ambrassade.jeugdlink.exceptions.NotFoundException;
import be.ambrassade.jeugdlink.model.Organisation;
import be.ambrassade.jeugdlink.model.Tag;
import be.ambrassade.jeugdlink.model.response.AuthenticatedProfileResponse;
import be.ambrassade.jeugdlink.model.response.ImageResponse;
import be.ambrassade.jeugdlink.model.response.ProfileResponse;
import be.ambrassade.jeugdlink.model.response.UserResponse;
import be.ambrassade.jeugdlink.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class UserControllerTest {
    @Mock
    private UserService userService;

    @InjectMocks
    private UserController controller;

    private UserResponse userResponse;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

        Organisation organisation = new Organisation();
        organisation.setName("org");
        organisation.setJeugdwerk(true);
        organisation.setId(2);

        Tag tag = new Tag();
        Tag tag2 = new Tag();
        Tag tag3 = new Tag();
        Set<Tag> tags = new HashSet<>();
        tags.add(tag);
        tags.add(tag2);
        tags.add(tag3);

        userResponse = new UserResponse();
        userResponse.setId(1);
        userResponse.setFirstName("first");
        userResponse.setLastName("last");
        userResponse.setImageName("test.jpg");
        userResponse.setOrganisation(organisation);
        userResponse.setOneliner("I am a oneliner");
        userResponse.setTags(tags);
    }


    @Test
    void getProfileReturnsProfileResponse() throws Exception {
        ProfileResponse response = new ProfileResponse();
        response.setId(1);
        response.setFirstName("ik");

        when(userService.getUserProfile(1)).thenReturn(response);

        mockMvc.perform(get("/users/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(1)));
    }

    @Test
    void getProfileNotExistingIdThrowsNotFound() throws Exception {
        when(userService.getUserProfile(anyInt())).thenThrow(new NotFoundException());

        mockMvc.perform(get("/users/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    void emailVerificationShouldPass() throws Exception {
        when(userService.verifyEmailActivation(eq(1), eq("testseed"), ArgumentMatchers.any(HttpServletRequest.class)))
                .thenReturn(true);

        mockMvc.perform(get("/users/emailverification/1")
                .param("seed", "testseed"))
                .andExpect(status().isOk());
    }

    @Test
    void emailVerificationFalseSeedShouldFail() throws Exception {
        when(userService.verifyEmailActivation(anyInt(), anyString(), ArgumentMatchers.any(HttpServletRequest.class)))
                .thenThrow(new NotFoundException());

        mockMvc.perform(get("/users/emailverification/1")
                .param("seed", "fakeseed"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getImageReturnsImageResponse() throws Exception {
        ImageResponse imageResponse = new ImageResponse(new byte[5]);
        when(userService.getImage(anyInt())).thenReturn(imageResponse);

        mockMvc.perform(get("/users/1/image"))
                .andExpect(status().isOk());
    }

    @Test
    void getMeReturnsUserObject() throws Exception {
        AuthenticatedProfileResponse user = new AuthenticatedProfileResponse();
        user.setId(1);
        user.setFirstName("ik");
        user.setEmail("ik@lol.be");

        when(userService.getCurrentUserProfile()).thenReturn(user);

        mockMvc.perform(get("/users/me"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(jsonPath("$.email").value(user.getEmail()));
    }
}
