/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.repo;

import be.ambrassade.jeugdlink.model.Organisation;
import org.springframework.beans.factory.annotation.Autowired;

public class OrganisationRepositoryImpl implements OrganisationRepositoryCust {

    @Autowired
    private OrganisationRepository organisationRepository;

    public Organisation findByNameOrCreate(String organisationName) {
        Organisation org = organisationRepository.findByNameIgnoreCase(organisationName);

        if (org == null) {
            org = new Organisation(organisationName);
            organisationRepository.save(org);
        }
        return org;
    }
}
