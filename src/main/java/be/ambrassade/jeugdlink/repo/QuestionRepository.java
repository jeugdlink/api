/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.repo;

import be.ambrassade.jeugdlink.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface QuestionRepository extends JpaRepository<Question, Integer> {
    Page<Question> findAllByActiveTrueOrderByPostedOnDesc(Pageable pageable);
    Page<Question> findAllByActiveTrueOrderByViewedDescPostedOnDesc(Pageable pageable);
    Page<Question> findAllByActiveTrueAndAnsweredTrueOrderByPostedOnDesc(Pageable pageable);

    @Query(value = "select * from vragen where actief = true and vraag ILIKE %?1%", nativeQuery = true)
    Page<Question> findAllBySearchTerm(String question, Pageable pageable);

    @Query(value = "select v.* from vragen_tags " +
            "left join vragen v on vraag_id = v.id " +
            "where tag_id = ?1 and v.actief = true " +
            "order by v.gepost_op desc",
            nativeQuery = true)
    Page<Question> findAllActiveByTagIdOrderByNew(int tagId, Pageable pageable);

    @Query(value = "select v.* from vragen_tags " +
            "left join vragen v on vraag_id = v.id " +
            "where tag_id = ?1 and v.actief = true " +
            "order by v.bekeken desc, v.gepost_op desc",
            nativeQuery = true)
    Page<Question> findAllActiveByTagIdOrderByPopularAndNew(int tagId, Pageable pageable);

    @Query(value = "select v.* from vragen_tags " +
            "left join vragen v on vraag_id = v.id " +
            "where tag_id = ?1 and v.actief = true and v.beantwoord = true " +
            "order by v.bekeken desc, v.gepost_op desc",
            nativeQuery = true)
    Page<Question> findAllActiveAndAnsweredByTagIdOrderByNew(int tagId, Pageable pageable);

    @Query(value = "select v.* from vragen_tags " +
            "left join vragen v on vraag_id = v.id " +
            "where tag_id = ?1 and v.actief = true and v.beantwoord = true", nativeQuery = true)
    Page<Question> findAllActiveAnsweredByTagId(int tagId, Pageable pageable);

    @Transactional
    @Modifying
    @Query("UPDATE Question q SET q.viewed = q.viewed + 1 WHERE q.id = ?1")
    void raiseViewedByOne(Integer id);

    @Transactional
    @Modifying
    @Query("UPDATE Question q SET q.answered = true WHERE q.id = ?1")
    void markQuestionAsAnswered(Integer id);
}
