/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.repo;

import be.ambrassade.jeugdlink.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    Page<Message> getBySender(int senderId, Pageable pageable);
    Page<Message> getByRecipientAndRecipientDeletedFalse(int recipientId, Pageable pageable);

    @Query(value = "select count(id) from berichten b where b.ontvanger = ?1 and b.gelezen_op is null",
            nativeQuery = true)
    int getUnreadMessagesCount(int recipientId);

    @Transactional
    @Modifying
    @Query("update Message m set m.recipientDeleted = true where m.id = ?1")
    void deleteByIdForRecipient(int id);

    @Transactional
    @Modifying
    @Query("update Message m set m.read = CURRENT_TIMESTAMP where m.id = ?1")
    void markAsRead(int id);
}
