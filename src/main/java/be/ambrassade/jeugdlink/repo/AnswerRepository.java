/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.repo;

import be.ambrassade.jeugdlink.model.Answer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    boolean existsByIdAndQuestionId(Integer id, Integer qid);

    @Query(value = "SELECT *, NULL as likes FROM antwoorden WHERE id = ?1", nativeQuery = true)
    Optional<Answer> findById(Integer id);

    @Query(value =
            "SELECT a.*, COUNT(al) AS likes FROM antwoorden a " +
            "LEFT JOIN antwoorden_likes al " +
            "ON a.id = al.antwoord " +
            "WHERE a.vraag = ?1 AND a.actief = TRUE " +
            "GROUP BY a.id " +
            "ORDER BY a.is_verkozen DESC, likes DESC, a.gepost_op",
            countQuery = "SELECT COUNT(*) FROM antwoorden WHERE vraag = ?1",
            nativeQuery = true)
    Page<Answer> getAnswersAndLikesByQuestionId(int questionId, Pageable pageable);

    @Query(value = "SELECT ?1 IN (SELECT gebruiker FROM antwoorden_likes WHERE antwoord = ?2)", nativeQuery = true)
    boolean hasUserLikedAnswer(int uid, int aid);

    @Transactional
    @Modifying
    @Query("UPDATE Answer a SET a.chosen = true WHERE a.id = ?1")
    void markAnswerAsChosen(Integer id);

    @Transactional
    @Modifying
    @Query("UPDATE Answer a SET a.chosen = false WHERE a.question.id = ?1")
    void resetChosenAnswer(Integer questionId);

    @Transactional
    @Modifying
    @Query(value =
            "INSERT INTO antwoorden_likes select ?1, ?2 " +
            "WHERE NOT EXISTS(SELECT * FROM antwoorden_likes WHERE gebruiker = ?1 AND antwoord = ?2)", nativeQuery = true)
    void doLike(int uid, int aid);
}
