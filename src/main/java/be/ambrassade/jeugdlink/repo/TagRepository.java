/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.repo;

import be.ambrassade.jeugdlink.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    Set<Tag> findTagsByUsersId(int id);
    Set<Tag> findTagsByQuestionsId(int id);
    Tag findByNameIgnoreCase(String name);
    Set<Tag> findTagsByNameIgnoreCaseIn(Set<String> names);

    @Query(value = "select * from top_tags limit ?1", nativeQuery = true)
    Set<Tag> getTopTags(int limit);
}
