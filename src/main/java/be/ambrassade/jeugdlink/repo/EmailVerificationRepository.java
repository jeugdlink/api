/*
 * Copyright (c) 2018-2019 Leonardo Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.repo;

import be.ambrassade.jeugdlink.model.EmailVerification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface EmailVerificationRepository extends CrudRepository<EmailVerification, Integer> {

    @Query(value = "select * from email_verificaties where gestart + '48 hours' < now() and geverifieerd_op is null",
            nativeQuery = true)
    Set<EmailVerification> getAllExpiredUnverifiedForEmailChanges();

    @Query(value = "select * from email_verificaties where gestart + '7 days' < now() " +
            "and geverifieerd_op is null and nieuwe_email is null",
            nativeQuery = true)
    Set<EmailVerification> getAllExpiredUnverifiedForNewAccounts();
}
