/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.repo;

import be.ambrassade.jeugdlink.model.User;
import be.ambrassade.jeugdlink.model.projection.NamesOnly;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Integer> {
    Page<User> findAllByDeactivatedFalse(Pageable pageable);
    List<User> findByFirstName(String firstName);
    List<User> findByLastName(String lastName);
    boolean existsByEmail(String email);

    @Query("select u.firstName as firstName, u.lastName as lastName from User u where id = ?1")
    NamesOnly getFullNameById(int id);

    @Query("from User u where lower(u.email) = lower(?1)")
    Optional<User> findOneByEmail(String email);

    @Query(value = "select afbeelding from gebruikers where id = ?1", nativeQuery = true)
    byte[] findImageByUserId(int id);

    @Transactional
    @Modifying
    @Query(value = "update gebruikers set afbeelding = ?2 where id = ?1", nativeQuery = true)
    void saveImageForUser(int id, byte[] image);

    @Transactional
    @Modifying
    @Query(value = "update gebruikers set voornaam = '[verwijderd]', achternaam = null, geslacht = null, " +
            "email = random(), wachtwoord = '', organisatie = null, afbeelding = null, telnr = null, " +
            "is_gedeactiveerd = true, gedeactiveerd_op = now(), oneliner = null, beschrijving = null, " +
            "afbeeldingnaam = null, nieuwe_email = null where id in ?1", nativeQuery = true)
    void anonymizeAccountsByIds(Set<Integer> ids);

    @Transactional
    @Modifying
    @Query(value = "update gebruikers set nieuwe_email = null where id in ?1", nativeQuery = true)
    void removeNewEmailAddressForUserIds(Set<Integer> ids);
}
