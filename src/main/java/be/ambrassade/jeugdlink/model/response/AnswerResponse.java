/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.response;

import java.sql.Timestamp;

public class AnswerResponse extends BaseResponse {
    private int id;
    private String message;
    private UserResponse user;
    private Timestamp postedOn;
    private boolean chosen;
    private Short likes;
    private boolean userLikedAnswer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserResponse getUser() {
        return user;
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    public Timestamp getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(Timestamp postedOn) {
        this.postedOn = postedOn;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public Short getLikes() {
        return likes;
    }

    public void setLikes(Short likes) {
        this.likes = likes;
    }

    public boolean isUserLikedAnswer() {
        return userLikedAnswer;
    }

    public void setUserLikedAnswer(boolean userLikedAnswer) {
        this.userLikedAnswer = userLikedAnswer;
    }
}
