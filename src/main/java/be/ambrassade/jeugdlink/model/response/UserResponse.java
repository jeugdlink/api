/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.response;

import be.ambrassade.jeugdlink.model.Organisation;
import be.ambrassade.jeugdlink.model.Tag;

import java.util.Set;

/**
 * De DTO dat het minimum van de zichtbare gegevens van gebruikers bevat
 */
public class UserResponse extends BaseResponse {
    private int id;
    private String firstName;
    private String lastName;
    private String imageName;
    private Organisation organisation;
    private String oneliner;
    private Set<Tag> tags;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public String getOneliner() {
        return oneliner;
    }

    public void setOneliner(String oneliner) {
        this.oneliner = oneliner;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}
