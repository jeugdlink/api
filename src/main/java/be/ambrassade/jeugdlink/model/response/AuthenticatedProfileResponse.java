/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.response;

/**
 * De DTO dat alle zichtbare gegevens van de geauthenticeerde gebruiker bevat
 */
public class AuthenticatedProfileResponse extends ProfileResponse {
    private boolean emailVisible;
    private boolean phoneNumberVisible;
    private boolean emailOnPm;
    private boolean emailOnAnswer;
    private boolean emailOnNews;
    private String newEmail;

    public boolean isEmailVisible() {
        return emailVisible;
    }

    public void setEmailVisible(boolean emailVisible) {
        this.emailVisible = emailVisible;
    }

    public boolean isPhoneNumberVisible() {
        return phoneNumberVisible;
    }

    public void setPhoneNumberVisible(boolean phoneNumberVisible) {
        this.phoneNumberVisible = phoneNumberVisible;
    }

    public boolean isEmailOnPm() {
        return emailOnPm;
    }

    public void setEmailOnPm(boolean emailOnPm) {
        this.emailOnPm = emailOnPm;
    }

    public boolean isEmailOnAnswer() {
        return emailOnAnswer;
    }

    public void setEmailOnAnswer(boolean emailOnAnswer) {
        this.emailOnAnswer = emailOnAnswer;
    }

    public boolean isEmailOnNews() {
        return emailOnNews;
    }

    public void setEmailOnNews(boolean emailOnNews) {
        this.emailOnNews = emailOnNews;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String emailToBeChanged) {
        this.newEmail = emailToBeChanged;
    }
}
