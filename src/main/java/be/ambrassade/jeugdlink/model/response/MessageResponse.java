/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.response;

import java.sql.Timestamp;

public class MessageResponse extends BaseResponse {
    private int id;
    private UserResponse partner;
    private String subject;
    private String message;
    private Timestamp sent;
    private Timestamp read;
    private Integer answerTo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserResponse getPartner() {
        return partner;
    }

    public void setPartner(UserResponse partner) {
        this.partner = partner;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getSent() {
        return sent;
    }

    public void setSent(Timestamp sent) {
        this.sent = sent;
    }

    public Timestamp getRead() {
        return read;
    }

    public void setRead(Timestamp read) {
        this.read = read;
    }

    public Integer getAnswerTo() {
        return answerTo;
    }

    public void setAnswerTo(Integer answerTo) {
        this.answerTo = answerTo;
    }
}
