/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tags")
public class Tag extends BaseEntity {
    @Column(name = "naam")
    private String name;

    @Column(name = "beschrijving")
    private String description;

    @Column(name = "is_vorming")
    private boolean vorming;

    @ManyToMany(mappedBy = "tags")
    private Set<Question> questions;

    @ManyToMany(mappedBy = "tags")
    private Set<User> users;


    public Tag() {}

    public Tag(String name) {
        this.name = name;
    }

    public Tag(String name, boolean vorming) {
        this.name = name;
        this.vorming = vorming;
    }

    public Tag(String name, boolean vorming, String description) {
        this.name = name;
        this.vorming = vorming;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVorming() {
        return vorming;
    }

    public void setVorming(boolean vorming) {
        this.vorming = vorming;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return vorming == tag.vorming &&
                Objects.equals(getId(), tag.getId()) &&
                Objects.equals(name, tag.name) &&
                Objects.equals(description, tag.description) &&
                Objects.equals(questions, tag.questions) &&
                Objects.equals(users, tag.users);
    }
}
