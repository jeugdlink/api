/*
 * Copyright (c) 2018-2019 Leonardo Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.projection;

public interface NamesOnly {
    String getFirstName();
    String getLastName();

    default String getFullName() {
        return getFirstName().concat(getLastName() != null ? " " + getLastName() : "");
    }
}
