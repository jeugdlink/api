/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class QuestionRequest {
    @Size(min = 12, max = 255)
    @NotNull
    private String question;

    @Size(max = 1000)
    private String message;

    private boolean answered;

    private String[] tags;


    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }
}
