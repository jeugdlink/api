/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.request;

import be.ambrassade.jeugdlink.validation.EmailUnique;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Moet gebruikt worden voor het aanmaken van een User.
 * Moeten tijdens deze request gegeven worden: voornaam, email, wachtwoord
 */
public class UserCreateRequest extends UserRequest {
    @Email
    @EmailUnique
    @NotNull
    private String email;

    @Size(min = 10, max = 72)
    @NotNull
    private String password;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
