/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model.request;

import be.ambrassade.jeugdlink.validation.PhoneNumberBelgium;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Alle gemeenschappelijke gebruikergegevens tijdens een request. Afbeelding maakt hier geen deel van u
 * it omdat die requests apart gebeuren.
 */
public abstract class UserRequest {
    @Size(min = 2, max = 50)
    @NotNull
    @Pattern(regexp = "^(?i)[a-z ,.'-]+$")
    private String firstName;

    @Size(min = 2, max = 50)
    @Pattern(regexp = "^(?i)[a-z ,.'-]*$")
    private String lastName;

    @Pattern(regexp = "^m|v|x$")
    private String gender;

    private String organisation;

    private String[] tags;

    @Size(min = 2, max = 58)
    private String oneliner;

    @Size(min = 25, max = 1500)
    private String description;

    private boolean emailVisible;
    private boolean phoneNumberVisible;

    private boolean emailOnPm;
    private boolean emailOnAnswer;
    private boolean emailOnNews;

    /**
     * Vast telefoonnummer of gsm-nummer in Belgisch formaat
     */
    @PhoneNumberBelgium
    private String phoneNumber;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getOneliner() {
        return oneliner;
    }

    public void setOneliner(String oneliner) {
        this.oneliner = oneliner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEmailVisible() {
        return emailVisible;
    }

    public void setEmailVisible(boolean emailVisible) {
        this.emailVisible = emailVisible;
    }

    public boolean isPhoneNumberVisible() {
        return phoneNumberVisible;
    }

    public void setPhoneNumberVisible(boolean phoneNumberVisible) {
        this.phoneNumberVisible = phoneNumberVisible;
    }

    public boolean isEmailOnPm() {
        return emailOnPm;
    }

    public void setEmailOnPm(boolean emailOnPm) {
        this.emailOnPm = emailOnPm;
    }

    public boolean isEmailOnAnswer() {
        return emailOnAnswer;
    }

    public void setEmailOnAnswer(boolean emailOnAnswer) {
        this.emailOnAnswer = emailOnAnswer;
    }

    public boolean isEmailOnNews() {
        return emailOnNews;
    }

    public void setEmailOnNews(boolean emailOnNews) {
        this.emailOnNews = emailOnNews;
    }
}
