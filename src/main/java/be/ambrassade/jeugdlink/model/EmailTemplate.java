/*
 * Copyright (c) 2018-2019 Leonardo Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;

import java.util.HashMap;
import java.util.Map;

public class EmailTemplate {
    private String to;
    private String subject;
    private String templateFile;
    private Map<String,String> placeholders;

    public EmailTemplate() { }

    public EmailTemplate(String to, String subject, String templateFile) {
        this.to = to;
        this.subject = subject;
        setTemplateFile(templateFile);
    }

    public EmailTemplate(String to, String subject, String templateFile, Map<String, String> placeholders) {
        this.to = to;
        this.subject = subject;
        this.placeholders = placeholders;
        setTemplateFile(templateFile);
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTemplateFile() {
        return templateFile;
    }

    public void setTemplateFile(String templateFile) {
        this.templateFile = templateFile + ".email";
    }

    public Map<String, String> getPlaceholders() {
        return placeholders;
    }

    public void setPlaceholders(Map<String, String> placeholders) {
        this.placeholders = placeholders;
    }

    public void addPlaceholder(String key, String value) {
        if (placeholders == null) {
            placeholders = new HashMap<>();
        }
        placeholders.put(key, value);
    }

    public void removePlaceholder(String key) {
        if (placeholders == null) {
            return;
        }
        placeholders.remove(key);
    }
}
