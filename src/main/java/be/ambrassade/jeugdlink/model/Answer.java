/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "antwoorden")
public class Answer extends BaseEntity {
    @Column(name = "bericht")
    private String message;

    @ManyToOne
    @JoinColumn(name = "gebruiker")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vraag")
    private Question question;

    @Column(name = "gepost_op", insertable = false)
    private Timestamp postedOn;

    @Column(name = "actief", insertable = false)
    private boolean active;

    @Column(name = "is_verkozen", insertable = false)
    private boolean chosen;

    /**
     * likes is een count() teruggave van getAnswersAndLikesByQuestionId()
     * @see be.ambrassade.jeugdlink.repo.AnswerRepository
     */
    @Column(insertable = false, updatable = false)

    private Short likes;

    public Answer() {}

    public  Answer(String message, User user) {
        this.message = message;
        this.user = user;
    }

    public Answer(String message, User user, Question question) {
        this.message = message;
        this.user = user;
        this.question = question;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Timestamp getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(Timestamp postedOn) {
        this.postedOn = postedOn;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public Short getLikes() {
        return likes;
    }

    public void setLikes(Short likes) {
        this.likes = likes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return active == answer.active &&
                chosen == answer.chosen &&
                Objects.equals(getId(), answer.getId()) &&
                Objects.equals(message, answer.message) &&
                Objects.equals(user, answer.user) &&
                Objects.equals(question, answer.question) &&
                Objects.equals(postedOn, answer.postedOn) &&
                Objects.equals(likes, answer.likes);
    }
}
