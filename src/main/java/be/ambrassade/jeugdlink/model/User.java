/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

/**
 * Niet rechtstreeks doorgeven aan View laag, want bevat gevoelige informatie.
 * Map in de plaats naar User/ProfileResponse
 */
@Entity
@Table(name = "gebruikers")
public class User extends BaseEntity implements UserDetails {
    @Column(name = "voornaam")
    private String firstName;

    @Column(name = "achternaam")
    private String lastName;

    @Column(name = "geslacht")
    private String gender;

    private String email;

    @Column(name = "wachtwoord")
    private String password;

    @ManyToOne
    @JoinColumn(name = "organisatie")
    private Organisation organisation;

    @Column(name = "afbeeldingnaam")
    private String imageName;

    @Column(name = "telnr")
    private String phoneNumber;

    private String oneliner;

    @Column(name = "beschrijving")
    private String description;

    @Column(name = "is_email_geactiveerd", insertable = false)
    private boolean emailActivated;

    /**
     * The new e-mail address is temporarily stored during the verification process
     */
    @Column(name = "nieuwe_email", insertable = false)
    private String newEmail;

    @Column(name = "geregistreerd_op", insertable = false, updatable = false)
    private Timestamp registeredOn;

    @JsonIgnore
    @Column(name = "is_gedeactiveerd", insertable = false)
    private boolean deactivated;

    @Column(name = "gedeactiveerd_op", insertable = false)
    private Timestamp deactivatedOn;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "gebruikers_tags",
            joinColumns = @JoinColumn(name = "gebruiker_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tags;

    @Column(name = "is_email_zichtbaar", insertable = false)
    private boolean emailVisible;

    @Column(name = "is_telnr_zichtbaar", insertable = false)
    private boolean phoneNumberVisible;

    @Column(name = "email_bij_pm", insertable = false)
    private boolean emailOnPm;

    @Column(name = "email_bij_antwoord", insertable = false)
    private boolean emailOnAnswer;

    @Column(name = "email_bij_nieuws", insertable = false)
    private boolean emailOnNews;

    @Transient
    private SecureRandom random = new SecureRandom();


    public User() {}

    public User(String firstName, String email, String password) {
        this.firstName = firstName;
        this.email = email;
        setPassword(password);
    }

    public User(String firstName, String email, String password, String lastName,
                String gender, String phoneNumber) {
        this.firstName = firstName;
        this.email = email;
        setPassword(password);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder(8, random).encode(password);
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOneliner() {
        return oneliner;
    }

    public void setOneliner(String oneliner) {
        this.oneliner = oneliner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isEmailActivated();
    }

    public boolean isEmailActivated() {
        return emailActivated;
    }

    public void setEmailActivated(boolean emailActivated) {
        this.emailActivated = emailActivated;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String emailToBeChanged) {
        this.newEmail = emailToBeChanged;
    }

    /**
     * Enkel getter want de databank registreert de timestamp bij het aanmaken van de user
     */
    public Timestamp getRegisteredOn() {
        return registeredOn;
    }

    @Override
    public boolean isEnabled() {
        return !isDeactivated();
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }

    public Timestamp getDeactivatedOn() {
        return deactivatedOn;
    }

    public void setDeactivatedOn(Timestamp deactivatedOn) {
        this.deactivatedOn = deactivatedOn;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public boolean isEmailVisible() {
        return emailVisible;
    }

    public void setEmailVisible(boolean emailVisible) {
        this.emailVisible = emailVisible;
    }

    public boolean isPhoneNumberVisible() {
        return phoneNumberVisible;
    }

    public void setPhoneNumberVisible(boolean phoneNumberVisible) {
        this.phoneNumberVisible = phoneNumberVisible;
    }

    public boolean isEmailOnPm() {
        return emailOnPm;
    }

    public void setEmailOnPm(boolean emailOnPm) {
        this.emailOnPm = emailOnPm;
    }

    public boolean isEmailOnAnswer() {
        return emailOnAnswer;
    }

    public void setEmailOnAnswer(boolean emailOnAnswer) {
        this.emailOnAnswer = emailOnAnswer;
    }

    public boolean isEmailOnNews() {
        return emailOnNews;
    }

    public void setEmailOnNews(boolean emailOnNews) {
        this.emailOnNews = emailOnNews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return emailActivated == user.emailActivated &&
                deactivated == user.deactivated &&
                emailVisible == user.emailVisible &&
                phoneNumberVisible == user.phoneNumberVisible &&
                emailOnPm == user.emailOnPm &&
                emailOnNews == user.emailOnNews &&
                emailOnAnswer == user.emailOnAnswer &&
                Objects.equals(getId(), user.getId()) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(gender, user.gender) &&
                Objects.equals(email, user.email) &&
                Objects.equals(newEmail, user.newEmail) &&
                Objects.equals(password, user.password) &&
                Objects.equals(organisation, user.organisation) &&
                Objects.equals(imageName, user.imageName) &&
                Objects.equals(phoneNumber, user.phoneNumber) &&
                Objects.equals(registeredOn, user.registeredOn) &&
                Objects.equals(deactivatedOn, user.deactivatedOn) &&
                Objects.equals(description, user.description) &&
                Objects.equals(oneliner, user.oneliner) &&
                Objects.equals(tags, user.tags);
    }
}
