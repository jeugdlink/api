/*
 * Copyright (c) 2018-2019 Leonardo Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "email_verificaties")
public class EmailVerification extends BaseEntity {
    @Column(name = "gestart", insertable = false, updatable = false)
    private Timestamp started;

    @Column(name = "geverifieerd_op", insertable = false)
    private Timestamp verifiedOn;

    @Column(name = "hash", insertable = false, updatable = false)
    @Generated(GenerationTime.INSERT)
    private String seed;

    @Column(name = "nieuwe_email", updatable = false)
    private String newEmail;

    @ManyToOne
    @JoinColumn(name = "gebruiker", updatable = false)
    private User user;

    private String ip;

    public EmailVerification() { }

    public EmailVerification(User user) {
        this.user = user;
    }

    public EmailVerification(String newEmail, User user) {
        this.newEmail = newEmail;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Timestamp getStarted() {
        return started;
    }

    public void setStarted(Timestamp started) {
        this.started = started;
    }

    public Timestamp getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(Timestamp verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}