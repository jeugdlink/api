/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "organisaties")
public class Organisation extends BaseEntity {
    @Column(name = "naam")
    private String name;

    @Column(name = "is_jeugdwerk", insertable = false)
    private boolean jeugdwerk;

    public Organisation() {}

    public Organisation(String name) {
        this.name = name;
    }

    public Organisation(String name, boolean jeugdwerk) {
        this.name = name;
        this.jeugdwerk = jeugdwerk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isJeugdwerk() {
        return jeugdwerk;
    }

    public void setJeugdwerk(boolean jeugdwerk) {
        this.jeugdwerk = jeugdwerk;
    }
}
