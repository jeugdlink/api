/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "vragen")
public class Question extends BaseEntity {
    @Column(name = "vraag")
    private String question;

    @Column(name = "bericht")
    private String message;

    @ManyToOne
    @JoinColumn(name = "gebruiker")
    private User user;

    @Column(name = "beantwoord", insertable = false)
    private boolean answered;

    @Column(name = "gepost_op", insertable = false)
    private Timestamp postedOn;

    @Column(name = "actief", insertable = false)
    private boolean active;

    @Column(name = "bekeken", insertable = false, updatable = false)
    private int viewed;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "vragen_tags",
            joinColumns = @JoinColumn(name = "vraag_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tags;

    @OneToMany(mappedBy = "question")
    @LazyCollection(LazyCollectionOption.EXTRA)
    private Set<Answer> answers;


    public Question() {}

    public Question(String question, String message, User user) {
        this.question = question;
        this.message = message;
        this.user = user;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }

    /**
     * Enkel getter want db genereert deze waarde automatisch
     */
    public Timestamp getPostedOn() {
        return postedOn;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getViewed() {
        return viewed;
    }

    public void setViewed(int viewed) {
        this.viewed = viewed;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return answered == question.answered &&
                active == question.active &&
                viewed == question.viewed &&
                Objects.equals(getId(), question.getId()) &&
                Objects.equals(this.question, question.question) &&
                Objects.equals(message, question.message) &&
                Objects.equals(user, question.user) &&
                Objects.equals(postedOn, question.postedOn) &&
                Objects.equals(tags, question.tags) &&
                Objects.equals(answers, question.answers);
    }
}
