/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "berichten")
public class Message extends BaseEntity {
    @Column(name = "zender")
    private Integer sender;

    @Column(name = "ontvanger")
    private Integer recipient;

    @Column(name = "onderwerp")
    private String subject;

    @Column(name = "bericht")
    private String message;

    @Column(name = "verstuurd_op", insertable = false, updatable = false)
    private Timestamp sent;

    @Column(name = "gelezen_op", insertable = false)
    private Timestamp read;

    @Column(name = "zender_verwijderd", insertable = false)
    private boolean senderDeleted;

    @Column(name = "ontvanger_verwijderd", insertable = false)
    private boolean recipientDeleted;

    /**
     * Bevat de ID van het bericht waarop dit bericht een antwoord is
     */
    @Column(name = "antwoord_op", updatable = false)
    private Integer answerTo;

    public Message(){}

    public Integer getSender() {
        return sender;
    }

    public void setSender(Integer sender) {
        this.sender = sender;
    }

    public Integer getRecipient() {
        return recipient;
    }

    public void setRecipient(Integer recipient) {
        this.recipient = recipient;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getSent() {
        return sent;
    }

    public void setSent(Timestamp sent) {
        this.sent = sent;
    }

    public Timestamp getRead() {
        return read;
    }

    public void setRead(Timestamp read) {
        this.read = read;
    }

    public boolean isSenderDeleted() {
        return senderDeleted;
    }

    public void setSenderDeleted(boolean senderDeleted) {
        this.senderDeleted = senderDeleted;
    }

    public boolean isRecipientDeleted() {
        return recipientDeleted;
    }

    public void setRecipientDeleted(boolean recipientDeleted) {
        this.recipientDeleted = recipientDeleted;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getAnswerTo() {
        return answerTo;
    }

    public void setAnswerTo(Integer answerTo) {
        this.answerTo = answerTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return senderDeleted == message.senderDeleted &&
                recipientDeleted == message.recipientDeleted &&
                Objects.equals(getId(), message.getId()) &&
                Objects.equals(sender, message.sender) &&
                Objects.equals(recipient, message.recipient) &&
                Objects.equals(subject, message.subject) &&
                Objects.equals(this.message, message.message) &&
                Objects.equals(sent, message.sent) &&
                Objects.equals(read, message.read) &&
                Objects.equals(answerTo, message.answerTo);
    }
}
