/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.mapper;

import be.ambrassade.jeugdlink.model.User;
import be.ambrassade.jeugdlink.model.request.UserRequest;
import be.ambrassade.jeugdlink.model.response.AuthenticatedProfileResponse;
import be.ambrassade.jeugdlink.model.response.ImageResponse;
import be.ambrassade.jeugdlink.model.response.ProfileResponse;
import be.ambrassade.jeugdlink.model.response.UserResponse;
import be.ambrassade.jeugdlink.service.TagService;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserResponse toUserResponse(User user);

    @Mapping(target = "email", expression = "java(!user.isEmailVisible() ? null : user.getEmail())")
    @Mapping(target = "phoneNumber", expression = "java(!user.isPhoneNumberVisible() ? null : user.getPhoneNumber())")
    ProfileResponse toProfileResponse(User user);

    AuthenticatedProfileResponse toAuthenticatedProfileResponse(User user);

    @Mapping(target = "tags", expression = "java(tagService.getTagsByNamesOrCreate(request.getTags()))")
    // Handled by UserServiceImpl.class
    @Mapping(target = "email", ignore = true)
    @Mapping(target = "organisation", ignore = true)
    @Mapping(target = "password", ignore = true)
    User userRequestToUser(UserRequest request, @MappingTarget User user, @Context TagService tagService);

    default ImageResponse toImageReponse(byte[] imageBlob) {
        return new ImageResponse(imageBlob);
    }
}
