/*
 * Copyright (c) 2018-2019 Leonardo Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.mapper;

import be.ambrassade.jeugdlink.model.EmailTemplate;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.util.regex.Pattern;

@Mapper
public interface EmailMapper {
    EmailMapper INSTANCE = Mappers.getMapper(EmailMapper.class);

    default SimpleMailMessage emailTemplateToSimpleMessage(EmailTemplate emailTemplate, String fromAddress)
            throws IOException {
        final String[] template = {
                Files.readString(
                        ResourceUtils.getFile("classpath:" + emailTemplate.getTemplateFile()).toPath())
        };
        emailTemplate.getPlaceholders().forEach(
                (k,v) -> template[0] = template[0].replaceAll(Pattern.quote(k), v != null ? v : "")
        );

        SimpleMailMessage emailMessage = new SimpleMailMessage();
        emailMessage.setTo(emailTemplate.getTo());
        emailMessage.setText(template[0]);
        emailMessage.setSubject(emailTemplate.getSubject());
        emailMessage.setFrom(fromAddress);

        return emailMessage;
    }
}
