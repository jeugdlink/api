/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.mapper;

import be.ambrassade.jeugdlink.model.Message;
import be.ambrassade.jeugdlink.model.request.MessageReplyRequest;
import be.ambrassade.jeugdlink.model.request.MessageRequest;
import be.ambrassade.jeugdlink.model.response.MessageResponse;
import be.ambrassade.jeugdlink.service.UserService;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MessageMapper {
    MessageMapper INSTANCE = Mappers.getMapper(MessageMapper.class);

    Message toMessage(MessageRequest messageRequest);

    @Mapping(target = "sender", expression = "java(userService.getAuthenticatedUser().getId())")
    Message toMessage(MessageReplyRequest messageReplyRequest, @Context UserService userService);

    @Mapping(target = "partner", expression = "java(userService.getUserLimitedDetails(message.getSender()))")
    MessageResponse toInboxMessageResponse(Message message, @Context UserService userService);

    @Mapping(target = "partner", expression = "java(userService.getUserLimitedDetails(message.getRecipient()))")
    MessageResponse toOutboxMessageResponse(Message message, @Context UserService userService);
}
