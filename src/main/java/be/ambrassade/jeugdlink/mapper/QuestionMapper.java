/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.mapper;

import be.ambrassade.jeugdlink.model.Question;
import be.ambrassade.jeugdlink.model.request.QuestionRequest;
import be.ambrassade.jeugdlink.model.response.QuestionResponse;
import be.ambrassade.jeugdlink.service.TagService;
import be.ambrassade.jeugdlink.service.UserService;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface QuestionMapper {
    QuestionMapper INSTANCE = Mappers.getMapper(QuestionMapper.class);

    @Mapping(target = "totalAnswers", expression = "java(question.getAnswers() != null ? question.getAnswers().size() : 0)")
    QuestionResponse questionToQuestionResponse(Question question);

    @Mapping(target = "tags", expression = "java(tagService.getTagsByNamesOrCreate(request.getTags()))")
    Question editQuestionRequestToQuestion(QuestionRequest request, @MappingTarget Question question, @Context TagService tagService);

    @Mapping(target = "user", expression = "java(userService.getAuthenticatedUser())")
    @Mapping(target = "tags", expression = "java(tagService.getTagsByNamesOrCreate(request.getTags()))")
    Question questionRequestToQuestion(QuestionRequest request, @Context UserService userService, @Context TagService tagService);

    List<QuestionResponse> questionsPageToQuestionResponseList(Page<Question> questions);
}
