/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink;

import be.ambrassade.jeugdlink.model.EmailTemplate;
import be.ambrassade.jeugdlink.model.EmailVerification;
import be.ambrassade.jeugdlink.model.User;
import be.ambrassade.jeugdlink.repo.EmailVerificationRepository;
import be.ambrassade.jeugdlink.service.EmailService;
import be.ambrassade.jeugdlink.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class ScheduledTasks {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final UserService userService;
    private final EmailVerificationRepository emailVerificationRepository;
    private final EmailService emailService;

    @Value("${spring.application.name}")
    private String APP_NAME;

    public ScheduledTasks(UserService userService,
                          EmailVerificationRepository emailVerificationRepository,
                          EmailService emailService) {
        this.userService = userService;
        this.emailVerificationRepository = emailVerificationRepository;
        this.emailService = emailService;
    }

    // Each 24 hours at 01h00
    @Scheduled(cron = "0 0 1 * * *")
    public void cronResetCachedAuthenticatedUserObjects() {
        //userService.resetCachedAuthenticatedUserObject();
        logger.info("cronResetCachedAuthenticatedUserObjects: Cron job has been executed");
    }

    // Each six hours
    @Scheduled(cron = "0 0 6 * * *")
    public void resetUnverifiedEmailAddressChangesAfter24Hours() {
        Set<EmailVerification> set = emailVerificationRepository.getAllExpiredUnverifiedForEmailChanges();
        Set<Integer> userIds = new HashSet<>();

        for (EmailVerification emailVerification : set) {
            User user = emailVerification.getUser();

            if (user.getNewEmail() != null) {
                try {
                    Map<String,String> placeholders = new HashMap<>();
                    placeholders.put("{firstname}", user.getFirstName());
                    placeholders.put("{lastname}", user.getLastName());
                    placeholders.put("{appname}", APP_NAME);
                    placeholders.put("{newemail}", user.getNewEmail());

                    EmailTemplate emailTemplate = new EmailTemplate(
                            user.getEmail(),
                            "Jouw nieuw e-mailadres was niet geverifieerd",
                            "verification-editemail-expired",
                            placeholders
                    );
                    emailService.sendEmail(emailTemplate);
                    userIds.add(user.getId());
                } catch (IOException e) {
                    logger.error("resetUnverifiedEmailAddressChangesAfter24Hours: {}", e.getMessage());
                }
            }
        }

        if (userIds.size() != 0) {
            userService.removeUnverifiedEmailAddresses(userIds);
        }
        logger.info("resetUnverifiedEmailAddressChangesAfter24Hours: Cron job has been executed");
    }

    // Each day
    @Scheduled(cron = "0 0 0 * * *")
    public void removeUnverifiedNewAccountsAfter7Days() {
        Set<EmailVerification> set = emailVerificationRepository.getAllExpiredUnverifiedForNewAccounts();
        Set<Integer> userIds = new HashSet<>();

        // TODO: send mail before removal

        for (EmailVerification emailVerification : set) {
            User user = emailVerification.getUser();

            if (!user.isDeactivated()) {
                try {
                    Map<String,String> placeholders = new HashMap<>();
                    placeholders.put("{firstname}", user.getFirstName());
                    placeholders.put("{appname}", APP_NAME);

                    EmailTemplate emailTemplate = new EmailTemplate(
                            user.getEmail(),
                            "Jouw e-mailadres was niet geverifieerd",
                            "verification-newaccount-expired",
                            placeholders
                    );
                    emailService.sendEmail(emailTemplate);
                    userIds.add(user.getId());
                } catch (IOException e) {
                    logger.error("removeUnverifiedNewAccountsAfter7Days: {}", e.getMessage());
                }
            }
        }

        if (userIds.size() != 0) {
            userService.removeNonActivatedAccounts(userIds);
        }
        logger.info("removeUnverifiedNewAccountsAfter7Days: Cron job has been executed");
    }
}
