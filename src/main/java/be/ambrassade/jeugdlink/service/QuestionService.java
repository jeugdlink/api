package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.Question;
import be.ambrassade.jeugdlink.model.request.QuestionRequest;
import be.ambrassade.jeugdlink.model.response.QuestionResponse;
import be.ambrassade.jeugdlink.repo.QuestionRepository;
import org.springframework.data.domain.Page;

public interface QuestionService {
    Page<QuestionResponse> getAllQuestions(int page, int size, String filter, Integer tagId);
    Page<QuestionResponse> searchForQuestions(String term, int page, int size);
    Page<QuestionResponse> searchForQuestionsByTagId(int tagId, int page, int size);
    Page<QuestionResponse> searchForPopularQuestionsByTagId(int tagId, int page, int size);
    Page<QuestionResponse> searchForAnsweredQuestionsByTagId(int tagId, int page, int size);
    QuestionResponse getQuestionAndRaiseViewsByOne(int id);
    QuestionResponse askQuestion(QuestionRequest request);
    void editQuestion(int id, QuestionRequest request);
    void deleteQuestion(int id);
    void deleteTagFromQuestion(int questionId, int tagId);
    void markQuestionAsAnswered(Integer id);
    Question getQuestionForModificationIfPermittedOrThrowException(int id);
    Question getQuestionIfExistsOrThrowException(int id);
    QuestionRepository getRepository();
}
