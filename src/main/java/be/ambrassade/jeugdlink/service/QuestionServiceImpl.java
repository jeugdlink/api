/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.exceptions.BadRequestException;
import be.ambrassade.jeugdlink.exceptions.ForbiddenException;
import be.ambrassade.jeugdlink.exceptions.NotFoundException;
import be.ambrassade.jeugdlink.mapper.QuestionMapper;
import be.ambrassade.jeugdlink.model.Question;
import be.ambrassade.jeugdlink.model.Tag;
import be.ambrassade.jeugdlink.model.User;
import be.ambrassade.jeugdlink.model.request.QuestionRequest;
import be.ambrassade.jeugdlink.model.response.QuestionResponse;
import be.ambrassade.jeugdlink.repo.QuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final UserService userService;
    private final TagService tagService;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    public QuestionServiceImpl(QuestionRepository questionRepository, UserService userService, TagService tagService) {
        this.questionRepository = questionRepository;
        this.userService = userService;
        this.tagService = tagService;
    }

    /**
     * Haal alle vragen op, gesorteerd om datum gevraagd desc. Mogelijkheid om te filteren
     *
     * @param page Pagination pagina
     * @param size Pagination aantal elementen
     * @param filter Filteren op nieuwe, populaire of beantwoorde vragen
     * @param tagId Filteren op tag
     * @return Antwoorden en pagination gegevens
     */
    public Page<QuestionResponse> getAllQuestions(int page, int size, String filter, Integer tagId) {
        Page<Question> questionPage;

        switch (filter) {
            case "popular":
                questionPage = tagId != null ?
                        questionRepository.findAllActiveByTagIdOrderByPopularAndNew(tagId, PageRequest.of(page, size)) :
                        questionRepository.findAllByActiveTrueOrderByViewedDescPostedOnDesc(PageRequest.of(page, size));
                break;

            case "answered":
                questionPage = tagId != null ?
                        questionRepository.findAllActiveAndAnsweredByTagIdOrderByNew(tagId, PageRequest.of(page, size)) :
                        questionRepository.findAllByActiveTrueAndAnsweredTrueOrderByPostedOnDesc(PageRequest.of(page, size));
                break;

            default:
                questionPage = tagId != null ?
                        questionRepository.findAllActiveByTagIdOrderByNew(tagId, PageRequest.of(page, size)) :
                        questionRepository.findAllByActiveTrueOrderByPostedOnDesc(PageRequest.of(page, size));
                break;
        }
        return new PageImpl<>(QuestionMapper.INSTANCE.questionsPageToQuestionResponseList(questionPage),
                questionPage.getPageable(),
                questionPage.getTotalElements());
    }

    @Override
    public Page<QuestionResponse> searchForQuestions(String term, int page, int size) {
        Page<Question> questions = questionRepository.findAllBySearchTerm(term, PageRequest.of(page, size));

        return new PageImpl<>(
                QuestionMapper.INSTANCE.questionsPageToQuestionResponseList(questions),
                questions.getPageable(),
                questions.getTotalElements()
        );
    }

    @Override
    public Page<QuestionResponse> searchForQuestionsByTagId(int tagId, int page, int size) {
        return getAllQuestions(page, size, "none", tagId);
    }

    @Override
    public Page<QuestionResponse> searchForPopularQuestionsByTagId(int tagId, int page, int size) {
        return getAllQuestions(page, size, "popular", tagId);
    }

    @Override
    public Page<QuestionResponse> searchForAnsweredQuestionsByTagId(int tagId, int page, int size) {
        return getAllQuestions(page, size, "answered", tagId);
    }

    /**
     * Haal de vraag op en zijn antwoorden en verhoog aantal keren bekeken met 1
     */
    @Override
    public QuestionResponse getQuestionAndRaiseViewsByOne(int id) {
        questionRepository.raiseViewedByOne(id);
        return QuestionMapper.INSTANCE.questionToQuestionResponse(getQuestionIfExistsOrThrowException(id));
    }

    /**
     * Voeg een nieuwe vraag toe aan de db. De db voorkomt dat vragen dubbel keer worden toegevoegd
     * door de unique constraint. Tags worden van strings omgezet in Tag objecten
     */
    public QuestionResponse askQuestion(QuestionRequest request) {
        Question question = QuestionMapper.INSTANCE.questionRequestToQuestion(request, userService, tagService);

        if (request.getTags() != null && request.getTags().length > 5) {
            throw new BadRequestException("Max 5 tags toegelaten");
        }
        questionRepository.save(question);
        logger.info("Question {} was added to the database", question.getId());
        return QuestionMapper.INSTANCE.questionToQuestionResponse(question);
    }

    public void editQuestion(int id, QuestionRequest request) {
        if (request.getTags() != null && request.getTags().length > 5) {
            throw new BadRequestException("Max 5 tags toegelaten");
        }
        Question question = getQuestionForModificationIfPermittedOrThrowException(id);
        questionRepository.save(QuestionMapper.INSTANCE.editQuestionRequestToQuestion(request, question, tagService));
        logger.info("Question {} edited successfully", id);
    }

    /**
     * Deactiveer de vraag zodat deze niet meer op te roepen valt.
     *
     * TODO: Moet na een bepaalde periode verwijderd worden
     */
    public void deleteQuestion(int id) {
        Question question = getQuestionForModificationIfPermittedOrThrowException(id);
        question.setActive(false);
        //question.setTags(null);
        questionRepository.save(question);

        logger.info("Question {} was deactivated", id);
    }

    /**
     * Verwijder tag van vraag. Enkel toegelaten door eigenaar
     */
    public void deleteTagFromQuestion(int questionId, int tagId) {
        Question question = getQuestionForModificationIfPermittedOrThrowException(questionId);
        Tag tag = tagService.getTag(tagId);

        if (tag != null) {
            question.getTags().remove(tag);
            questionRepository.save(question);
            logger.info("Tag {} removed from question {}", tagId, questionId);
        }
    }

    /**
     * Markeer vraag als beantwoord
     */
    public void markQuestionAsAnswered(Integer id) {
        questionRepository.markQuestionAsAnswered(id);
    }

    /**
     * Haal de vraag op als de gebruiker ook de eigenaar is, anders wordt er een ForiddenException teruggegeven
     */
    @Override
    public Question getQuestionForModificationIfPermittedOrThrowException(int id) {
        Question question = getQuestionIfExistsOrThrowException(id);
        User user = userService.getAuthenticatedUser();

        if (!question.getUser().getId().equals(user.getId())) {
            logger.warn("User {} tried to perform an action on question {}, but he's not the owner!", user.getEmail(), id);
            throw new ForbiddenException();
        }
        logger.info("User {} has full access to question {}", user.getId(), id);
        return question;
    }

    /**
     * Kijk of de vraag bestaat, gooi anders een NotFoundException
     */
    @Override
    public Question getQuestionIfExistsOrThrowException(int id) {
        Question question = questionRepository.findById(id).orElse(null);

        if (question == null || !question.isActive()) {
            logger.warn("Question {} called by user {} but it's non-existing or inactive", id, userService.getAuthenticatedUser().getId());
            throw new NotFoundException();
        }
        return question;
    }

    public QuestionRepository getRepository() {
        return questionRepository;
    }
}
