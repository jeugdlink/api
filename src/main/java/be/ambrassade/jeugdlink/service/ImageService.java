/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import java.io.IOException;

public interface ImageService {
    void loadImage(byte[] imageBuffer);
    byte[] convertToAvatar() throws IOException;
    String saveToFile(String filename) throws IOException;
    void deleteCachedFile(String filenameWithExtension);
    IMAGE_TYPE getImageType(byte[] image) throws IOException;
    IMAGE_TYPE getSourceImageType() throws IOException;
    IMAGE_TYPE getResultImageType() throws IOException;
}
