package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.Tag;
import be.ambrassade.jeugdlink.repo.TagRepository;

import java.util.Set;

public interface TagService {
    Tag getTag(int id);
    Set<Tag> getAllTags();
    Set<Tag> getTagsForUser(int id);
    Set<Tag> getTagsForQuestion(int id);
    Set<Tag> getTagsByNames(String[] names);
    Set<Tag> getTagsByNamesOrCreate(String[] names);
    Set<Tag> getTop5Tags();
    Tag addTag(Tag tag);
    void editTag(int id);
    TagRepository getRepository();
}
