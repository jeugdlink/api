/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import com.tinify.Options;
import com.tinify.Source;
import com.tinify.Tinify;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;

enum IMAGE_TYPE {
    UNKNOWN, JPG, PNG
}

@Service
public class ImageServiceImpl implements ImageService {
    @Value("${com.tinify.apikey}")
    private String API_KEY;

    @Value("${be.ambrassade.jeugdlink.avatars.cachepath}")
    private String IMAGE_CACHE_PATH;

    @Value("${be.ambrassade.jeugdlink.avatars.method}")
    private String IMAGE_OPTIMIZE_METHOD;

    @Value("${be.ambrassade.jeugdlink.avatars.width}")
    private int IMAGE_WIDTH;

    @Value("${be.ambrassade.jeugdlink.avatars.height}")
    private int IMAGE_HEIGHT;

    private Source source;
    private Source resultSource;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void loadImage(byte[] imageBuffer) {
        Tinify.setKey(API_KEY);
        source = Tinify.fromBuffer(imageBuffer);
        logger.info(getClass() + ": Image buffer loaded");
    }

    @Override
    public byte[] convertToAvatar() throws IOException {
        Options options = new Options()
                .with("method", "thumb")
                .with("width", IMAGE_WIDTH)
                .with("height", IMAGE_HEIGHT);

        resultSource = source.resize(options);
        logger.info(getClass() + ": Image resized successfully");
        return resultSource.toBuffer();
    }

    @Override
    public String saveToFile(String filename) throws IOException {
        String extension = getResultImageType() == IMAGE_TYPE.PNG ? ".png" : ".jpg";
        resultSource.toFile(String.format("%s/%s%s", IMAGE_CACHE_PATH, filename, extension));
        logger.info(getClass() + ": Image saved to file");
        return filename + extension;
    }

    @Override
    public void deleteCachedFile(String filenameWithExtension) {
        File file = new File(String.format("%s/%s", IMAGE_CACHE_PATH, filenameWithExtension));

        if (file.delete()) {
            logger.info("Image file {} has been deleted", filenameWithExtension);
        } else {
            logger.warn("Image file {} could not be deleted", filenameWithExtension);
        }
    }

    @Override
    public IMAGE_TYPE getImageType(byte[] image) throws IOException {
        DataInputStream data = new DataInputStream(new ByteArrayInputStream(image));
        int magicNumber = data.readInt();

        switch (magicNumber) {
            case 0xffd8ffe0:
                return IMAGE_TYPE.JPG;

            case 0x89504E47:
                return IMAGE_TYPE.PNG;

                default:
                    return IMAGE_TYPE.UNKNOWN;
        }
    }

    @Override
    public IMAGE_TYPE getSourceImageType() throws IOException {
        return getImageType(source.toBuffer());
    }

    @Override
    public IMAGE_TYPE getResultImageType() throws IOException {
        return getImageType(resultSource.toBuffer());
    }
}
