/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.exceptions.ForbiddenException;
import be.ambrassade.jeugdlink.exceptions.NotFoundException;
import be.ambrassade.jeugdlink.mapper.MessageMapper;
import be.ambrassade.jeugdlink.model.Message;
import be.ambrassade.jeugdlink.model.User;
import be.ambrassade.jeugdlink.model.request.MessageReplyRequest;
import be.ambrassade.jeugdlink.model.request.MessageRequest;
import be.ambrassade.jeugdlink.model.response.MessageResponse;
import be.ambrassade.jeugdlink.model.response.UnreadMessageCountResponse;
import be.ambrassade.jeugdlink.repo.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final EmailService emailService;

    public MessageServiceImpl(MessageRepository messageRepository,
                              UserService userService,
                              EmailService emailService) {
        this.messageRepository = messageRepository;
        this.userService = userService;
        this.emailService = emailService;
    }

    @Override
    public void createMessage(MessageRequest messageRequest) {
        Message message = MessageMapper.INSTANCE.toMessage(messageRequest);
        // TODO: naar mapper
        message.setSender(userService.getAuthenticatedUser().getId());
        messageRepository.save(message);
        logger.info("createMessage: User {} successfully sent message to User {}", message.getSender(), message.getRecipient());

        User authenticatedUser = userService.getAuthenticatedUser();
        User recipient = message.getRecipient().equals(authenticatedUser.getId())
                ? authenticatedUser
                : userService.getOtherUser(message.getRecipient());

        // Skip email for self pm
        if (recipient.isEmailOnPm() && !recipient.getId().equals(authenticatedUser.getId())) {
            emailService.sendPmNotification(userService.getFullName(message.getSender()), recipient);
        }
    }

    @Override
    public void createReply(MessageReplyRequest messageRequest) {
        Message originalMessage = getMessageAsAuthenticated(messageRequest.getAnswerTo());
        Message replyMessage = MessageMapper.INSTANCE.toMessage(messageRequest, userService);
        replyMessage.setSubject(originalMessage.getSubject());
        replyMessage.setRecipient(originalMessage.getSender());
        messageRepository.save(replyMessage);
        logger.info("createReply: User {} replied succesfully to User {}", replyMessage.getSender(), replyMessage.getRecipient());

        User authenticatedUser = userService.getAuthenticatedUser();
        User recipient = replyMessage.getRecipient().equals(authenticatedUser.getId())
                ? authenticatedUser
                : userService.getOtherUser(replyMessage.getRecipient());
        // Skip email for self pm
        if (recipient.isEmailOnPm() && !recipient.getId().equals(authenticatedUser.getId())) {
            emailService.sendPmNotification(userService.getFullName(replyMessage.getSender()), recipient);
        }
    }

    @Override
    public MessageResponse getMessage(int id) {
        return MessageMapper.INSTANCE.toInboxMessageResponse(getMessageAsAuthenticated(id), userService);
    }

    /**
     * Just get the number of unread messages. This is used for polling for new messages
     */
    @Override
    public UnreadMessageCountResponse getTotalUnreadMessages() {
        UnreadMessageCountResponse response = new UnreadMessageCountResponse();
        response.setCount(messageRepository.getUnreadMessagesCount(userService.getAuthenticatedUser().getId()));
        return response;
    }

    @Override
    public void deleteMessageFromInbox(int id) {
        Message message = getMessageAsAuthenticated(id);

        if (!message.isRecipientDeleted()) {
            messageRepository.deleteByIdForRecipient(id);
        }
        logger.info("User {} deleted message {}", userService.getAuthenticatedUser().getId(), id);
    }

    @Override
    public Page<MessageResponse> getSentMessages(int page, int size) {
        Page<Message> messages = messageRepository.getBySender(
                userService.getAuthenticatedUser().getId(),
                PageRequest.of(page, size, Sort.Direction.DESC, "sent")
        );
        logger.info("Messages as sender loaded");

        return new PageImpl<>(messages.stream().map(m -> MessageMapper.INSTANCE.toOutboxMessageResponse(m, userService))
                .collect(Collectors.toList()), messages.getPageable(), messages.getTotalElements());
    }

    @Override
    public Page<MessageResponse> getReceivedMessages(int page, int size) {
        Page<Message> messages = messageRepository.getByRecipientAndRecipientDeletedFalse(
                userService.getAuthenticatedUser().getId(),
                PageRequest.of(page, size, Sort.Direction.DESC, "sent")
        );
        logger.info("Messages as recipient loaded");

        return new PageImpl<>(messages.stream().map(m -> MessageMapper.INSTANCE.toInboxMessageResponse(m, userService))
                .collect(Collectors.toList()), messages.getPageable(), messages.getTotalElements());
    }

    @Override
    public void markAsRead(int id) {
        Message message = getMessageAsAuthenticated(id);

        if (message.getRead() == null) {
            messageRepository.markAsRead(id);
        }
    }

    /**
     * Haal bericht op als huidige gebruiker ontvanger is
     *
     * @throws NotFoundException Bericht bestaat niet
     * @throws ForbiddenException Gebruiker is niet ontvanger van het bericht
     */
    private Message getMessageAsAuthenticated(int id) {
        Message message = messageRepository.findById(id).orElse(null);
        User user = userService.getAuthenticatedUser();

        if (message == null || message.isRecipientDeleted()) {
            logger.warn("getMessageAsAuthenticated({}): user '{}' tried to open non-existing message", id, user.getId());
            throw new NotFoundException();
        } else if (!message.getRecipient().equals(user.getId())) {
            logger.warn("getMessageAsAuthenticated({}): user '{}' doesn't have permission to open message!", id, user.getId());
            throw new ForbiddenException();
        }
        return message;
    }
}
