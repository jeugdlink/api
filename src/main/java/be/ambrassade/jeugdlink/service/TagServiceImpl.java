/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.Tag;
import be.ambrassade.jeugdlink.repo.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {
    private final TagRepository tagRepository;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Set<Tag> getAllTags() {
        return new HashSet<>(tagRepository.findAll());
    }

    public Tag getTag(int id) {
        return tagRepository.findById(id).orElse(null);
    }

    @Override
    public Set<Tag> getTop5Tags() {
        return tagRepository.getTopTags(5);
    }

    public Set<Tag> getTagsByNames(String[] names) {
        return tagRepository.findTagsByNameIgnoreCaseIn(Set.of(names));
    }

    public Set<Tag> getTagsByNamesOrCreate(String[] names) {
        if (names == null) {
            return null;
        }

        String[] deduppedNames = Arrays.stream(names).distinct().map(String::toLowerCase).toArray(String[]::new);
        Set<Tag> tags = getTagsByNames(deduppedNames);
        Set<String> tagNames = tags.stream().map(Tag::getName).collect(Collectors.toSet());

        logger.info("Question will get these tags: {}", String.join(",", deduppedNames));

        Arrays.stream(deduppedNames).forEach(name -> {
            if (!tagNames.contains(name)) {
                Tag tag = tagRepository.save(new Tag(name));
                tags.add(tag);
                logger.info("Tag {} was created and added to the question", name);
            }
        });
        return tags;
    }

    @Override
    public Set<Tag> getTagsForUser(int userId) {
        return tagRepository.findTagsByUsersId(userId);
    }

    @Override
    public Set<Tag> getTagsForQuestion(int questionId) {
        return tagRepository.findTagsByQuestionsId(questionId);
    }

    public Tag addTag(Tag tag) {
        tag.setName(tag.getName().toLowerCase());

        Tag savedTag = tagRepository.save(tag);
        logger.info("New tag {} created in the database", savedTag.getId());

        return savedTag;
    }

    public void editTag(int id) { }

    public TagRepository getRepository() {
        return tagRepository;
    }
}
