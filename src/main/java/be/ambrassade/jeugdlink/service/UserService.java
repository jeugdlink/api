package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.User;
import be.ambrassade.jeugdlink.model.request.UserCreateRequest;
import be.ambrassade.jeugdlink.model.request.UserEditRequest;
import be.ambrassade.jeugdlink.model.response.AuthenticatedProfileResponse;
import be.ambrassade.jeugdlink.model.response.ImageResponse;
import be.ambrassade.jeugdlink.model.response.ProfileResponse;
import be.ambrassade.jeugdlink.model.response.UserResponse;
import be.ambrassade.jeugdlink.repo.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public interface UserService {
    Page<UserResponse> getAllUsers(int page, int size, String orderBy, String order);
    UserResponse getUserLimitedDetails(int id);
    ProfileResponse getUserProfile(int id);
    AuthenticatedProfileResponse getCurrentUserProfile();
    ProfileResponse getOtherUserProfile(int id);
    User getOtherUser(int id);
    User getAuthenticatedUser();
    String getFullName(User user);
    String getFullName(int userId);
    //void resetCachedAuthenticatedUserObject();
    AuthenticatedProfileResponse createUser(UserCreateRequest req);
    AuthenticatedProfileResponse editUser(int id, UserEditRequest req);
    User loadUserByUsername(String email);
    void deleteUser(int id);
    void removeNonActivatedAccounts(Set<Integer> ids);
    void removeUnverifiedEmailAddresses(Set<Integer> ids);
    void deleteTagFromUser(int userId, int tagId);
    boolean verifyEmailActivation(int id, String seed, HttpServletRequest request);
    ImageResponse getImage(int id);
    void setImage(int id, MultipartFile afbeelding) throws Exception;
    void deleteImage(int id);
    UserRepository getRepository();
}
