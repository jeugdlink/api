package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.Organisation;
import be.ambrassade.jeugdlink.repo.OrganisationRepository;

import java.util.List;

public interface OrganisationService {
    List<Organisation> getAllOrganisations(int page, int size);
    OrganisationRepository getRepository();
}
