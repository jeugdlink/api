/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.Organisation;
import be.ambrassade.jeugdlink.repo.OrganisationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrganisationServiceImpl implements OrganisationService {

    private final OrganisationRepository organisationRepository;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    public OrganisationServiceImpl(OrganisationRepository organisationRepository) {
        this.organisationRepository = organisationRepository;
    }

    /**
     * Haal standaard tot 20 organisaties op
     */
    public List<Organisation> getAllOrganisations(int page, int size) {
        logger.info("getAllOrganisations({}, {}) called", page, size);

        List<Organisation> organisations = new ArrayList<>();

        if (organisationRepository.count() > 0) {
            organisationRepository.findAll(PageRequest.of(page, size)).forEach(organisations::add);
        }
        return organisations;
    }

    public OrganisationRepository getRepository() {
        return organisationRepository;
    }
}
