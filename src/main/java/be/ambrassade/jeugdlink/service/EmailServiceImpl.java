/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.mapper.EmailMapper;
import be.ambrassade.jeugdlink.model.EmailTemplate;
import be.ambrassade.jeugdlink.model.Question;
import be.ambrassade.jeugdlink.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final EmailMapper emailMapper;

    @Value("${spring.mail.username}")
    private String fromEmail;

    @Value("${spring.application.name}")
    private String APP_NAME;

    @Value("${be.ambrassade.jeugdlink.url.inbox}")
    private String APP_URL_INBOX;

    @Value("${be.ambrassade.jeugdlink.url.profile}")
    private String APP_URL_PROFILE;

    @Value("${be.ambrassade.jeugdlink.url.answers}")
    private String APP_URL_ANSWERS;

    public EmailServiceImpl(JavaMailSender javaMailSender, EmailMapper emailMapper) {
        this.javaMailSender = javaMailSender;
        this.emailMapper = emailMapper;
    }

    @Override
    public void sendEmail(EmailTemplate emailTemplate) {
        logger.info("sendEmail: preparing e-mail message");
        try {
            javaMailSender.send(emailMapper.emailTemplateToSimpleMessage(emailTemplate, fromEmail));
            logger.info("sendEmail: successfully sent");
        } catch (IOException e) {
            logger.error("sendEmail: error sending e-mail message");
        }
    }

    @Override
    public void sendPmNotification(String senderName, User recipient) {
        Map<String,String> placeholders = basicPlaceholdersMap();
        placeholders.put("{firstname}", recipient.getFirstName());
        placeholders.put("{sender}", senderName);
        placeholders.put("{url}", APP_URL_INBOX);

        sendEmail(new EmailTemplate(
                recipient.getEmail(),
                String.format("Je hebt een privébericht ontvangen op %s!", APP_NAME),
                "pmreceived",
                placeholders
        ));
        logger.info("sendPmNotification: e-mail sent to User {}", recipient.getId());
    }

    @Override
    public void sendAnswerNotification(Question question, User owner) {
        Map<String,String> placeholders = basicPlaceholdersMap();
        placeholders.put("{firstname}", owner.getFirstName());
        placeholders.put("{url}", APP_URL_ANSWERS + "?id=" + question.getId());
        placeholders.put("{question}", question.getQuestion());

        sendEmail(new EmailTemplate(
                owner.getEmail(),
                "Iemand heeft geantwoord op je vraag!",
                "replyreceived",
                placeholders
        ));
        logger.info("sendAnswerNotification: e-mail sent to User {}", owner.getId());
    }

    private Map<String,String> basicPlaceholdersMap() {
        Map<String,String> placeholders = new HashMap<>();
        placeholders.put("{appname}", APP_NAME);
        placeholders.put("{unsubscribe}", APP_URL_PROFILE);

        return placeholders;
    }
}
