/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.request.MessageReplyRequest;
import be.ambrassade.jeugdlink.model.request.MessageRequest;
import be.ambrassade.jeugdlink.model.response.MessageResponse;
import be.ambrassade.jeugdlink.model.response.UnreadMessageCountResponse;
import org.springframework.data.domain.Page;

public interface MessageService {
    void createMessage(MessageRequest message);
    void createReply(MessageReplyRequest message);
    MessageResponse getMessage(int id);
    UnreadMessageCountResponse getTotalUnreadMessages();
    void deleteMessageFromInbox(int id);
    Page<MessageResponse> getSentMessages(int page, int size);
    Page<MessageResponse> getReceivedMessages(int page, int size);
    void markAsRead(int id);
}
