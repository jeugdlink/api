/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.EmailTemplate;
import be.ambrassade.jeugdlink.model.Question;
import be.ambrassade.jeugdlink.model.User;

import java.io.IOException;

public interface EmailService {
    void sendEmail(EmailTemplate emailTemplate) throws IOException;
    void sendPmNotification(String senderName, User recipient);
    void sendAnswerNotification(Question question, User owner);
}
