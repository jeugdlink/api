/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.exceptions.ForbiddenException;
import be.ambrassade.jeugdlink.exceptions.NotFoundException;
import be.ambrassade.jeugdlink.mapper.AnswerMapper;
import be.ambrassade.jeugdlink.model.Answer;
import be.ambrassade.jeugdlink.model.Question;
import be.ambrassade.jeugdlink.model.request.AnswerRequest;
import be.ambrassade.jeugdlink.model.response.AnswerResponse;
import be.ambrassade.jeugdlink.repo.AnswerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;
    private final QuestionService questionService;
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final EmailService emailService;


    public AnswerServiceImpl(AnswerRepository answerRepository,
                             QuestionService questionService,
                             UserService userService,
                             EmailService emailService) {
        this.answerRepository = answerRepository;
        this.questionService = questionService;
        this.userService = userService;
        this.emailService = emailService;
    }

    /**
     * Haal alle antwoorden voor een vraag op, gesorteerd op datum desc
     */
    @Override
    public Page<AnswerResponse> getAllAnswersForQuestion(int id, int page, int size) {
        Page<Answer> answerPage = answerRepository.getAnswersAndLikesByQuestionId(id, PageRequest.of(page, size));
        List<AnswerResponse> answers = AnswerMapper.INSTANCE.answersPageToAnswerResponseList(answerPage);

        answers.forEach(a -> a.setUserLikedAnswer(
                answerRepository.hasUserLikedAnswer(userService.getAuthenticatedUser().getId(), a.getId())
        ));
        return new PageImpl<>(answers, answerPage.getPageable(), answerPage.getTotalElements());
    }

    /**
     * Sla een nieuw antwoord op. Eender welke gebruiker mag een antwoord plaatsen op eender welke vraag
     */
    @Override
    public AnswerResponse storeAnswer(int id, AnswerRequest request) {
        Question question = questionService.getQuestionIfExistsOrThrowException(id);
        Answer answer = new Answer(request.getMessage(), userService.getAuthenticatedUser(), question);
        answerRepository.save(answer);
        logger.info("New answer with id {} was saved", id);

        if (!question.getUser().getId().equals(userService.getAuthenticatedUser().getId())
                && question.getUser().isEmailOnAnswer()) {
            emailService.sendAnswerNotification(question, question.getUser());
        }
        return AnswerMapper.INSTANCE.answerToAnswerResponse(answer);
    }

    /**
     * Bewerk een antwoord. Mag enkel door eigenaar gebeuren, anders ForbiddenException. Een cross-check of Question id
     * wel klopt lijkt hier niet nuttig.
     */
    @Override
    public void editAnswer(int id, AnswerRequest request) {
        Answer answer = getAnswerForModificationIfPermittedOrThrowException(id);
        answer.setMessage(request.getMessage());
        answerRepository.save(answer);
        logger.info("Answer {} was edited by user {}", id, userService.getAuthenticatedUser().getId());
    }

    /**
     * Deactiveer een antwoord. Mag enkel door eigenaar
     *
     * TODO: Moet nog na x periode verwijderd worden
     */
    @Override
    public void deleteAnswer(int id) {
        Answer answer = getAnswerForModificationIfPermittedOrThrowException(id);
        answer.setActive(false);
        answerRepository.save(answer);
        logger.info("Answer {} was deactivated by user {}", id, userService.getAuthenticatedUser().getId());
    }

    /**
     * Markeer een antwoord die de vraag beantwoordt
     */
    @Override
    public void markAnswerForQuestion(int qid, int id) {
        Question question = questionService.getQuestionForModificationIfPermittedOrThrowException(qid);

        if (!answerRepository.existsByIdAndQuestionId(id, qid)) {
            logger.warn("markAnswerForQuestion: User {} tried to manipulate Question {} and Answer {}, which don't belong together",
                    userService.getAuthenticatedUser().getId(),
                    qid,
                    id);
            throw new NotFoundException();
        } else if (!question.getUser().getId().equals(userService.getAuthenticatedUser().getId())) {
            logger.warn("markAnswerForQuestion: User {} doesn't have permission to modify Question {}",
                    userService.getAuthenticatedUser().getId(),
                    qid);
            throw new ForbiddenException();
        }
        answerRepository.resetChosenAnswer(qid);
        answerRepository.markAnswerAsChosen(id);
        questionService.markQuestionAsAnswered(qid);
    }

    /**
     * Een antwoord liken
     */
    @Override
    public void likeAnswer(int id) {
        Answer answer = getAnswerIfExistsOrThrowException(id);

        // User cannot like own answer
        if (answer.getUser().getId().equals(userService.getAuthenticatedUser().getId())) {
            logger.info("User {} tried to like own Answer {}", userService.getAuthenticatedUser().getId(), id);
            throw new ForbiddenException();
        }
        answerRepository.doLike(userService.getAuthenticatedUser().getId(), id);
        logger.info("User {} liked Answer {}", userService.getAuthenticatedUser().getId(), id);
    }

    /**
     * Bestaat het antwoord? Geef het dan terug, gooi anders een NotFoundException
     */
    Answer getAnswerIfExistsOrThrowException(int id) {
        Answer answer = answerRepository.findById(id).orElse(null);

        if (answer == null || !answer.isActive()) {
            logger.warn("Answer {} called by user {} but it's non-existing or inactive",
                    id, userService.getAuthenticatedUser().getId());
            throw new NotFoundException();
        }
        logger.info("Answer {} was found", id);
        return answer;
    }

    /**
     * Haal het antwoord op als de gebruiker ook de eigenaar is, anders wordt er een ForiddenException teruggegeven
     */
    Answer getAnswerForModificationIfPermittedOrThrowException(int id) {
        Answer answer = getAnswerIfExistsOrThrowException(id);

        if (!answer.getUser().getId().equals(userService.getAuthenticatedUser().getId())) {
            logger.warn("User {} tried to modify answer {} but he was not the owner!",
                    userService.getAuthenticatedUser().getId(), id);
            throw new ForbiddenException();
        }
        logger.info("User {} has full access to answer {}", userService.getAuthenticatedUser().getId(), id);
        return answer;
    }

    public AnswerRepository getRepository() {
        return answerRepository;
    }
}
