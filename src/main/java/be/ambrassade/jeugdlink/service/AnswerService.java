package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.model.request.AnswerRequest;
import be.ambrassade.jeugdlink.model.response.AnswerResponse;
import be.ambrassade.jeugdlink.repo.AnswerRepository;
import org.springframework.data.domain.Page;

public interface AnswerService {
    Page<AnswerResponse> getAllAnswersForQuestion(int id, int page, int size);
    AnswerResponse storeAnswer(int id, AnswerRequest request);
    void editAnswer(int id, AnswerRequest request);
    void deleteAnswer(int id);
    void markAnswerForQuestion(int qid, int aid);
    void likeAnswer(int id);
    AnswerRepository getRepository();
}
