/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.service;

import be.ambrassade.jeugdlink.exceptions.ForbiddenException;
import be.ambrassade.jeugdlink.exceptions.NotFoundException;
import be.ambrassade.jeugdlink.mapper.UserMapper;
import be.ambrassade.jeugdlink.model.*;
import be.ambrassade.jeugdlink.model.request.UserCreateRequest;
import be.ambrassade.jeugdlink.model.request.UserEditRequest;
import be.ambrassade.jeugdlink.model.response.AuthenticatedProfileResponse;
import be.ambrassade.jeugdlink.model.response.ImageResponse;
import be.ambrassade.jeugdlink.model.response.ProfileResponse;
import be.ambrassade.jeugdlink.model.response.UserResponse;
import be.ambrassade.jeugdlink.repo.EmailVerificationRepository;
import be.ambrassade.jeugdlink.repo.OrganisationRepository;
import be.ambrassade.jeugdlink.repo.UserRepository;
import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final OrganisationRepository organisationRepository;
    private final EmailVerificationRepository emailVerificationRepository;
    private final EmailService emailService;
    private final TagService tagService;
    private final ImageService imageService;
    private final TransactionTemplate transactionTemplate;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final UserMapper userMapper = UserMapper.INSTANCE;

    @Value("${spring.application.name}")
    private String APP_NAME;

    @Value("${be.ambrassade.jeugdlink.emailverification}")
    private boolean emailVerification;

    @Value("${be.ambrassade.jeugdlink.url.emailverification}")
    private String APP_URL_EMAILVERIFICATION;

    /**
     * Zal de geauthenticeerde User objecten cachen
     */
    //private Map<String, User> authenticatedUserObjects = new HashMap<>();

    /**
     * User object geladen door de LoginService. Deze zal worden doorgegeven aan authenticatedUserObjects bij
     * een succesvolle login
     */
    //private User preAuthenticationUserObject;

    public UserServiceImpl(UserRepository userRepository,
                           OrganisationRepository organisationRepository,
                           EmailVerificationRepository emailVerificationRepository,
                           EmailService emailService,
                           PlatformTransactionManager transactionManager,
                           TagService tagService,
                           ImageService imageService) {
        this.userRepository = userRepository;
        this.organisationRepository = organisationRepository;
        this.emailVerificationRepository = emailVerificationRepository;
        this.emailService = emailService;
        this.transactionTemplate = new TransactionTemplate(transactionManager);
        this.tagService = tagService;
        this.imageService = imageService;
    }

    /**
     * Haal standaard tot 20 actieve gebruikers op. Ze worden beschreven als foreign user omdat privégegevens gefilterd
     * moeten worden. Enkel de eigenaar kan al zijn gegevens laden
     */
    @Override
    public Page<UserResponse> getAllUsers(int page, int size, String orderBy, String order) {
        Page<User> users = userRepository.findAllByDeactivatedFalse(
                PageRequest.of(page, size, order.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, orderBy)
        );

        logger.info("Page {} of foreign users loaded", page);

        return new PageImpl<>(users.stream().map(UserMapper.INSTANCE::toUserResponse)
                .collect(Collectors.toList()), users.getPageable(), users.getTotalElements());
    }

    /**
     * Haal beperkte gegevens op voor de gebruiker
     */
    @Override
    public UserResponse getUserLimitedDetails(int id) {
        return getAuthenticatedUser().getId().equals(id)
                ? userMapper.toUserResponse(getAuthenticatedUser())
                : userMapper.toUserResponse(getOtherUser(id));
    }

    /**
     * Haal de gebruiker op via zijn id. Volledige (zichtbare) gegevens indien het om de eigenaar gaat, anders beperkt
     *
     * @return ProfileResponse|AuthenticatedProfileResponse
     */
    @Override
    public ProfileResponse getUserProfile(int id) {
        return getAuthenticatedUser().getId().equals(id)
                ? getCurrentUserProfile()
                : getOtherUserProfile(id);
    }

    /**
     * Haal alle gegevens van de gebruiker terug (volledige zichtbaarheid). Mag enkel door de ingelogde gebruiker gebeuren.
     *
     * We moeten hier niet controller of de account gedeactiveerd is, want omdat het over de ingelogde gebruiker
     * gaat, zal de authenticatie vanaf te volgende request falen als de gebruiker zijn account verwijderd heeft
     */
    @Override
    public AuthenticatedProfileResponse getCurrentUserProfile() {
        return userMapper.toAuthenticatedProfileResponse(getCurrentUserIfPermitted(getAuthenticatedUser().getId()));
    }

    /**
     * Haal gegevens van een andere gebruiker op (beperkte zichtbaarheid)
     *
     * Verberg privégegevens (zichtbaarheid = false) door ze op null te zetten
     */
    @Override
    public ProfileResponse getOtherUserProfile(int id) {
        return userMapper.toProfileResponse(getOtherUser(id));
    }

    /**
     * Haal een andere gebruiker op.
     *
     * Dit is voor intern gebruik en niet om door te geven aan View layer!
     *
     * Voor de ingelogde gebruiker is er getAuthenticatedUser()
     * @see this.getAuthenticatedUser()
     */
    @Override
    public User getOtherUser(int id) {
        User user = userRepository.findById(id).orElse(null);

        if (user == null) {
            logger.error("getOtherUser: user {} not found", id);
            throw new NotFoundException();
        } else if (user.getId().equals(getAuthenticatedUser().getId())) {
            logger.error("getOtherUser: This method is not meant to get the authenticated user");
            throw new ForbiddenException();
        }
        return user;
    }

    /**
     * Haal de gebruikersgegevens op obv de authentication username (e-mailadres).
     *
     * Dit is voor intern gebruik en niet om door te geven aan View layer!
     */
    @Override
    public User getAuthenticatedUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        return userRepository.findOneByEmail(username).orElseThrow();

        /*if (authenticatedUserObjects == null || authenticatedUserObjects.get(username) == null) {
            logger.info("Cache MISS; User {} was put into authenticatedUserObjects", preAuthenticationUserObject.getId());
            authenticatedUserObjects.put(username, preAuthenticationUserObject);
            preAuthenticationUserObject = null;
        } else {
            logger.info("Cache HIT for User {} in authenticatedUserObjects", authenticatedUserObjects.get(username).getId());
        }
        return authenticatedUserObjects.get(username);*/
    }

    @Override
    public String getFullName(User user) {
        return user.getFirstName().concat(user.getLastName() != null ? " " + user.getLastName() : "");
    }

    @Override
    public String getFullName(int userId) {
        return userRepository.getFullNameById(userId).getFullName();
    }

    /**
     * Reset the cache so we can start anew, otherwise it'll only build up since a record is only removed when
     * the user deletes his account
     */
//    public void resetCachedAuthenticatedUserObject() {
//        logger.info("Reset authenticatedUserObjects cache");
//        authenticatedUserObjects = null;
//    }

    /**
     * Update the authenticated User object in the cache so it's always up-to-date. Should be used
     * after that User object was modified
     */
//    private void updateCachedAuthenticatedUserObject(User user) {
//        logger.info("User {} was updated in authenticatedUserObjects", user.getId());
//
//        if (authenticatedUserObjects.get(user.getEmail()) != null) {
//            authenticatedUserObjects.replace(user.getEmail(), user);
//        }
//    }

    /**
     * Remove the cached User object
     */
//    private void removeCachedAuthenticatedUserObject(User user) {
//        logger.info("User {} was removed authenticatedUserObjects", user.getId());
//        authenticatedUserObjects.remove(user.getEmail());
//    }

    /**
     * User aanmaken en organisatie aanmaken indien een naam is meegegeven en deze nog niet in de
     * db bestaat. Verstuurt ook de e-mail om het e-mailadres te valideren
     *
     * @return AuthenticatedProfileResponse object of HTTP error als het fout gaat
     */
    @Override
    public AuthenticatedProfileResponse createUser(UserCreateRequest req) {
        User user = userMapper.userRequestToUser(req, new User(), tagService);
        user.setEmail(req.getEmail().toLowerCase());
        user.setPassword(req.getPassword());

        User result = setUserAndOrganisationTransaction(user, req.getOrganisation());
        logger.info("createUser: User {} successfully created", result.getId());

        if (emailVerification) {
            logger.info("createUser: E-mail needs to be verified");
            EmailVerification emailVerificationRecord = new EmailVerification(user);
            emailVerificationRepository.save(emailVerificationRecord);
            logger.info("createUser: E-mail verification record created");

            EmailTemplate emailTemplate = new EmailTemplate(
                    user.getEmail(),
                    "Verifieer je je e-mailadres even?",
                    "verification-newaccount"
            );
            // TODO: revert if mail sending failed
            sendVerificationEmail(emailTemplate, emailVerificationRecord);
        } else {
            logger.info("createUser: E-mail verification skipped because it's disabled in config");
        }
        return userMapper.toAuthenticatedProfileResponse(result);
    }

    /**
     * User wijzigen volgens de gegevens die zijn veranderd. Wachtwoord en e-mail dienen enkel gegeven te worden
     * indien deze aangepast moeten worden. Organisatie wordt ook aangemaakt indien nodig.
     * @return Aangepaste User of HTTP object als het fout gaat
     */
    @Override
    public AuthenticatedProfileResponse editUser(int id, UserEditRequest request) {
        User user = getCurrentUserIfPermitted(id);
        user = userMapper.userRequestToUser(request, user, tagService);

        if (request.getPassword() != null) {
            logger.info("editUser: User {} wants to change his password", id);
            user.setPassword(request.getPassword());
        }

        if (request.getEmail() != null) {
            logger.info("editUser: User {} wants to change his e-mail address", id);

            if (emailVerification) {
                logger.info("editUser: E-mail needs to be verified again");
                EmailVerification emailVerificationRecord = new EmailVerification(request.getEmail(), user);
                // TODO: revert if mail sending failed
                emailVerificationRepository.save(emailVerificationRecord);
                logger.info("editUser: E-mail verification record created");

                EmailTemplate emailTemplate = new EmailTemplate(
                        request.getEmail().toLowerCase(),
                        "Verifieer je je nieuw e-mailadres even?",
                        "verification-editedemail"
                );
                sendVerificationEmail(emailTemplate, emailVerificationRecord);
                user.setNewEmail(request.getEmail().toLowerCase());
            } else {
                logger.info("editUser: E-mail verification skipped because it's disabled in config");
            }
        }
        User updatedUser = setUserAndOrganisationTransaction(user, request.getOrganisation());
        //updateCachedAuthenticatedUserObject(updatedUser);

        return userMapper.toAuthenticatedProfileResponse(updatedUser);
    }

    /**
     * Opent een transactie en haalt een organisatie op of maakt die aan indien nodig en wordt dan
     * toegekend aan de user. Indien er een fout gebeurt, zal er een rollback gebeuren
     * @return User object die al dan niet aangepast is
     */
    private User setUserAndOrganisationTransaction(User user, String organisatie) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                if (organisatie != null) {
                    Organisation org = organisationRepository.findByNameOrCreate(organisatie);
                    user.setOrganisation(org);
                }
                userRepository.save(user);
            }
        });
        logger.info("User was successfully created/edited");

        return user;
    }

    private void sendVerificationEmail(EmailTemplate emailTemplate, EmailVerification emailVerification) {
        emailTemplate.addPlaceholder("{firstname}", emailVerification.getUser().getFirstName());
        emailTemplate.addPlaceholder("{lastname}", emailVerification.getUser().getLastName());
        emailTemplate.addPlaceholder("{appname}", APP_NAME);
        emailTemplate.addPlaceholder("{url}", APP_URL_EMAILVERIFICATION +
                String.format("?id=%d&seed=%s", emailVerification.getId(), emailVerification.getSeed()));

        try {
            emailService.sendEmail(emailTemplate);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public boolean verifyEmailActivation(int id, String seed, HttpServletRequest request) {
        EmailVerification emailVerification = emailVerificationRepository.findById(id).orElse(null);

        if (emailVerification != null && emailVerification.getVerifiedOn() == null) {
            User user = emailVerification.getUser();

            if (emailVerification.getSeed().equals(seed)
                    // Expired verification records, 48 hours for edits
                    && (user.getNewEmail() != null && isWithin48Hours(emailVerification.getStarted())
                        // 7 days for new accounts
                        || user.getNewEmail() == null && isWithin7Days(emailVerification.getStarted()))) {
                // Only needed when e-mail is changed, not when account is created
                if (emailVerification.getNewEmail() != null && emailVerification.getNewEmail().equals(user.getNewEmail())) {
                    user.setEmail(emailVerification.getNewEmail());
                    user.setNewEmail(null);
                }
                // E-mail edits shouldn't lock users out of their accounts, that's why emailActivated
                // remains true for edits, but not for new accounts
                user.setEmailActivated(true);
                userRepository.save(user);

                if (request.getHeader("X-Real-IP") != null && !request.getHeader("X-Real-IP").equals("")) {
                    emailVerification.setIp(request.getHeader("X-Real-IP"));
                } else {
                    emailVerification.setIp(request.getRemoteAddr());
                }
                emailVerification.setVerifiedOn(new Timestamp(System.currentTimeMillis()));
                emailVerificationRepository.save(emailVerification);

                return true;
            }
        }
        return false;
    }

    private boolean isWithin48Hours(Timestamp timestamp) {
        Timestamp timestamp1 = new Timestamp(timestamp.getTime() + (48 * 3600 * 1000));

        return timestamp.before(timestamp1);
    }

    private boolean isWithin7Days(Timestamp timestamp) {
        Timestamp timestamp1 = new Timestamp(timestamp.getTime() + (7 * 24 * 3600 * 1000));

        return timestamp.before(timestamp1);
    }

    @Override
    public ImageResponse getImage(int id) {
        // Jackson converteert byte[] automatisch naar base64 string
        return userMapper.toImageReponse(userRepository.findImageByUserId(id));
    }

    @Override
    public void setImage(int id, MultipartFile image) throws Exception {
        User user = getCurrentUserIfPermitted(id);
        char[] allowCharactersInNanoId = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();

        if (user != null) {
            imageService.loadImage(image.getBytes());
            // image is not part of User for better lazy loading possibilities
            userRepository.saveImageForUser(user.getId(), imageService.convertToAvatar());
            // Save to file so that nginx can service this file much more efficient since it's static data
            user.setImageName(imageService.saveToFile(NanoIdUtils.randomNanoId(new Random(), allowCharactersInNanoId, 6)));
            userRepository.save(user);
            logger.info("Image was set for user and saved to a file {}", id);
        }
    }

    @Override
    public void deleteImage(int id) {
        User user = getCurrentUserIfPermitted(id);
        imageService.deleteCachedFile(user.getImageName());
        userRepository.saveImageForUser(user.getId(), null);
        user.setImageName(null);
        userRepository.save(user);
        logger.info("Image deleted for user {}", id);
    }

    /**
     * Biedt functionaliteit aan om een gebruiker te deactiveren. Moet alle persoonlijke gegevens verwijderen:
     * afbeelding
     * namen
     * e-mailadres
     * organisatie
     * telnummer
     *
     * TODO: afwerken
     */
    @Override
    public void deleteUser(int id) {
        User user = getCurrentUserIfPermitted(id);

        if (user.isDeactivated()) {
            throw new NotFoundException();
        }
        user.setDeactivated(true);
        user.setDeactivatedOn(new Timestamp(System.currentTimeMillis()));
        user.setTags(null);

        // Om fraude te vermijden, moeten persoonlijke gegevens pas na een bepaalde periode gewist worden,
        // niet meteen bij de verwijderactie van de user
        /*user.setFirstName("");
        user.setLastName(null);
        user.setEmail("");
        user.setOrganisation(null);
        user.setPhoneNumber(null);
        user.setImage(null);*/

        userRepository.save(user);
        logger.warn("User {} deactivated", id);

        //removeCachedAuthenticatedUserObject(user);
    }

    /**
     * For use with the cron job
     *
     * @see be.ambrassade.jeugdlink.ScheduledTasks#removeUnverifiedNewAccountsAfter7Days()
     */
    @Override
    public void removeNonActivatedAccounts(Set<Integer> ids) {
        userRepository.anonymizeAccountsByIds(ids);
        logger.info("removeNonActivatedAccounts: Accounts {} have been anonymized", ids.toString());
    }

    @Override
    public void removeUnverifiedEmailAddresses(Set<Integer> ids) {
        userRepository.removeNewEmailAddressForUserIds(ids);
        logger.info("removeUnverifiedEmailAddresses: New e-mail address removed from accounts {}", ids.toString());
    }

    /**
     * Verwijder tag van gebruikersprofiel. Enkel toegelaten door eigenaar
     */
    @Override
    public void deleteTagFromUser(int userId, int tagId) {
        User user = getCurrentUserIfPermitted(userId);
        Tag tag = tagService.getTag(tagId);

        if (tag != null) {
            user.getTags().remove(tag);
            userRepository.save(user);
            logger.info("Tag {} removed from user {}", tagId, userId);
        }
    }

    /**
     * Geeft het huidige gebruiker object terug indien het ook de ingelogde gebruiker is en permissie heeft om
     * gevoelige handelingen te doen, anders zal het een ForbiddenException gooien.
     */
    private User getCurrentUserIfPermitted(int id) {
        User user = getAuthenticatedUser();

        if (!user.getId().equals(id)) {
            logger.warn("getCurrentUserIfPermitted({}): user '{}' tried to pretend to be user '{}'", id, user.getId(), id);
            throw new ForbiddenException();
        }
        return user;
    }

    /**
     * Deze methode wordt gebruikt door de LoginService voor authenticatie en cachet de User object
     *
     * Dit is voor intern gebruik en niet om door te geven aan View layer
     *
     * @see LoginService
     */
    @Override
    public User loadUserByUsername(String email) {
        return userRepository.findOneByEmail(email).orElse(null);
    }

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }
}
