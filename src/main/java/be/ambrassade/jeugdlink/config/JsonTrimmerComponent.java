package be.ambrassade.jeugdlink.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class JsonTrimmerComponent {
    public static class JsonTrimmerDeserializer extends JsonDeserializer<String> {

        @Override
        public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            String json = StringDeserializer.instance.deserialize(jsonParser, deserializationContext);
            return trim(json);
        }

        /**
         * Copyright Apache Software Foundation (ASF)
         * License: http://www.apache.org/licenses/LICENSE-2.0
         *
         * <p>Removes control characters (char &lt;= 32) from both
         * ends of this String, handling {@code null} by returning
         * {@code null}.</p>
         *
         * <p>The String is trimmed using {@link String#trim()}.
         * Trim removes start and end characters &lt;= 32.
         * To strip whitespace use {@link #strip(String)}.</p>
         *
         * <p>To trim your choice of characters, use the
         * {@link #strip(String, String)} methods.</p>
         *
         * <pre>
         * StringUtils.trim(null)          = null
         * StringUtils.trim("")            = ""
         * StringUtils.trim("     ")       = ""
         * StringUtils.trim("abc")         = "abc"
         * StringUtils.trim("    abc    ") = "abc"
         * </pre>
         *
         * @param str  the String to be trimmed, may be null
         * @return the trimmed string, {@code null} if null String input
         */
        public static String trim(final String str) {
            return str == null ? null : str.trim();
        }
    }
}
