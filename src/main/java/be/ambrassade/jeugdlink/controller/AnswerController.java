/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.controller;

import be.ambrassade.jeugdlink.exceptions.BadRequestException;
import be.ambrassade.jeugdlink.model.request.ActionRequest;
import be.ambrassade.jeugdlink.model.request.AnswerRequest;
import be.ambrassade.jeugdlink.model.response.AnswerResponse;
import be.ambrassade.jeugdlink.service.AnswerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AnswerController {

    private final AnswerService answerService;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }


    @GetMapping("/questions/{id}/answers")
    Page<AnswerResponse> getAllAnswersForQuestion(@PathVariable int id,
                                                  @RequestParam(required = false, defaultValue = "0") int page,
                                                  @RequestParam(required = false, defaultValue = "20") int size) {
        logger.info("getAllAnswersForQuestion({}, {}, {}) called", id, page, size);
        return answerService.getAllAnswersForQuestion(id, page, size);
    }


    @PostMapping("/questions/{id}/answers")
    ResponseEntity giveAnswer(@PathVariable int id, @Valid @RequestBody AnswerRequest request, BindingResult bindingResult) {
        logger.info("giveAnswer({}, ...) called", id);

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        return ResponseEntity.ok(answerService.storeAnswer(id, request));
    }


    @PutMapping("/answers/{id}")
    ResponseEntity editAnswer(@PathVariable int id, @Valid @RequestBody AnswerRequest request, BindingResult bindingResult) {
        logger.info("editAnswer({}) called", id);

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        answerService.editAnswer(id, request);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/questions/{qid}/answers/{id}")
    ResponseEntity markAnswer(@PathVariable int qid, @PathVariable int id,
                              @Valid @RequestBody ActionRequest actionRequest,
                              BindingResult bindingResult) {
        logger.info("markAnswer({}, {}, ...) called", qid, id);

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        switch (actionRequest.getAction())
        {
            case "markanswered":
                logger.info("Supported PATCH action triggered: markanswered");

                if (!actionRequest.getValue().equals("true")) {
                    throw new BadRequestException("Only value true is accepted");
                }
                answerService.markAnswerForQuestion(qid, id);
                break;

            case "likeanswer":
                logger.info("Supported PATCH action triggered: likeanswer");

                if (!actionRequest.getValue().equals("true")) {
                    throw new BadRequestException("Only value true is accepted");
                }
                answerService.likeAnswer(id);
                break;

            default:
                logger.warn("Unsupported PATCH action triggered: {}", actionRequest.getAction());
                throw new BadRequestException("Action not supported: " + actionRequest.getAction());
        }
        return ResponseEntity.ok().build();
    }


    @DeleteMapping("/answers/{id}")
    void deleteAnswer(@PathVariable int id) {
        logger.info("deleteAnswer({}) called", id);
        answerService.deleteAnswer(id);
    }
}
