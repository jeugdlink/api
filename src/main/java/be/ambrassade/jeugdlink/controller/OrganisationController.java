/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.controller;

import be.ambrassade.jeugdlink.model.Organisation;
import be.ambrassade.jeugdlink.service.OrganisationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping("/organisations")
public class OrganisationController {

    private final OrganisationService organisationService;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    public OrganisationController(OrganisationService organisationService) {
        this.organisationService = organisationService;
    }


    @GetMapping
    List<Organisation> getAllOrganisations(@RequestParam(required = false, defaultValue = "0") int page,
                                           @RequestParam(required = false, defaultValue = "20") int size) {
        logger.info("getAllOrganisations({}, {}) called", page, size);
        return organisationService.getAllOrganisations(page, size);
    }
}
