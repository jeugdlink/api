/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.controller;

import be.ambrassade.jeugdlink.exceptions.BadRequestException;
import be.ambrassade.jeugdlink.model.request.ActionRequest;
import be.ambrassade.jeugdlink.model.request.MessageReplyRequest;
import be.ambrassade.jeugdlink.model.request.MessageRequest;
import be.ambrassade.jeugdlink.model.response.MessageResponse;
import be.ambrassade.jeugdlink.model.response.UnreadMessageCountResponse;
import be.ambrassade.jeugdlink.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/messages")
public class MessageController {
    private final MessageService messageService;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    Page<MessageResponse> getInboxMessages(@RequestParam(required = false, defaultValue = "0") int page,
                                           @RequestParam(required = false, defaultValue = "20") int size,
                                           @RequestParam(required = false, defaultValue = "inbox") String filter) {
        logger.info("getInboxMessages({}, {}, {}) called", page, size, filter);

        return filter.equals("sent")
                ? messageService.getSentMessages(page, size)
                : messageService.getReceivedMessages(page, size);
    }

    @GetMapping("{id}")
    MessageResponse getMessage(@PathVariable int id) {
        logger.info("getMessage({}) called", id);
        return messageService.getMessage(id);
    }

    @GetMapping("unread")
    UnreadMessageCountResponse getUnread() {
        logger.info("getUnread() called");
        return messageService.getTotalUnreadMessages();
    }

    @PostMapping
    ResponseEntity storeMessage(@Valid @RequestBody MessageRequest messageRequest, BindingResult bindingResult) {
        logger.info("storeMessage(...) called");

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        messageService.createMessage(messageRequest);
        return ResponseEntity.ok().build();
    }

    @PostMapping("reply")
    ResponseEntity storeReply(@Valid @RequestBody MessageReplyRequest messageRequest, BindingResult bindingResult) {
        logger.info("storeReply(...) called");

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        messageService.createReply(messageRequest);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{id}")
    ResponseEntity deleteMessage(@PathVariable int id) {
        logger.info("deleteMessage({}) called", id);
        messageService.deleteMessageFromInbox(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("{id}")
    ResponseEntity markAsRead(@PathVariable int id, @Valid @RequestBody ActionRequest actionRequest, BindingResult bindingResult) {
        logger.info("markAsRead({}) called", id);

        if (bindingResult.hasErrors()) {
            logger.warn(String.format("There were %d validation errors!", bindingResult.getAllErrors().size()));
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        if (!actionRequest.getAction().equals("markasread")) {
            logger.warn("Unsupported PATCH action triggered: {}", actionRequest.getAction());
            throw new BadRequestException("Action not supported: " + actionRequest.getAction());
        } else {
            logger.info("Supported PATCH action triggered: markasread");

            if (!actionRequest.getValue().equals("true")) {
                throw new BadRequestException("Only value true is accepted");
            }
            messageService.markAsRead(id);
        }
        return ResponseEntity.ok().build();
    }

}
