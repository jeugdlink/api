/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.controller;

import be.ambrassade.jeugdlink.model.Tag;
import be.ambrassade.jeugdlink.service.QuestionService;
import be.ambrassade.jeugdlink.service.TagService;
import be.ambrassade.jeugdlink.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
public class TagController {
    private final TagService tagService;
    private final UserService userService;
    private final QuestionService questionService;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    public TagController(TagService tagService, UserService userService, QuestionService questionService) {
        this.tagService = tagService;
        this.userService = userService;
        this.questionService = questionService;
    }


    @GetMapping("/tags")
    Set<Tag> getAlleTags(@RequestParam(required = false, defaultValue = "none") String filter) {
        logger.info("getAlleTags() called");

        switch (filter) {
            case "top5":
                return tagService.getTop5Tags();

            default:
                return tagService.getAllTags();
        }
    }


    @GetMapping("/questions/{id}/tags")
    Set<Tag> getAllTagsForQuestion(@PathVariable int id) {
        logger.info("getAllTagsForQuestion({}) called", id);
        return tagService.getTagsForQuestion(id);
    }


    @GetMapping("/users/{id}/tags")
    Set<Tag> getAllTagsForUser(@PathVariable int id) {
        logger.info("getAllTagsForUser({}) called", id);
        return tagService.getTagsForUser(id);
    }


    @PostMapping("/tags")
    ResponseEntity addTag(@Valid @RequestBody Tag tag, BindingResult bindingResult) {
        logger.info("addTag() called");

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        return ResponseEntity.ok(tagService.addTag(tag));
    }


    @PutMapping("/tags/{id}")
    ResponseEntity editTag(@PathVariable int id, @Valid @RequestBody Tag tag, BindingResult bindingResult) {
        return null;
    }


    // TODO: US dep injection door setter
    @DeleteMapping("/users/{uid}/tags/{id}")
    void deleteTagForUser(@PathVariable int uid, @PathVariable int id) {
        logger.info("deleteTagForUser({}, {}) called", uid, id);
        userService.deleteTagFromUser(uid, id);
    }


    // TODO: QS dep injection door setter
    @DeleteMapping("/questions/{qid}/tags/{id}")
    void deleteTagForQuestion(@PathVariable int qid, @PathVariable int id) {
        logger.info("deleteTagForQuestion({}, {}) called", qid, id);
        questionService.deleteTagFromQuestion(qid, id);
    }


    @DeleteMapping("/tags/{id}")
    ResponseEntity deleteTag() {
        return null;
    }
}
