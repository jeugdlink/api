/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.controller;

import be.ambrassade.jeugdlink.exceptions.BadRequestException;
import be.ambrassade.jeugdlink.model.request.UserCreateRequest;
import be.ambrassade.jeugdlink.model.request.UserEditRequest;
import be.ambrassade.jeugdlink.model.response.ImageResponse;
import be.ambrassade.jeugdlink.model.response.ProfileResponse;
import be.ambrassade.jeugdlink.model.response.UserResponse;
import be.ambrassade.jeugdlink.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    Page<UserResponse> getAllUsers(@RequestParam(required = false, defaultValue = "0") int page,
                                   @RequestParam(required = false, defaultValue = "20") int size,
                                   @RequestParam(required = false, defaultValue = "firstname") String orderBy,
                                   @RequestParam(required = false, defaultValue = "asc") String order) {
        logger.info("getAllUsers({}, {}) called", page, size);

        switch (orderBy) {
            case "lastname":
                orderBy = "lastName";
                break;

            case "registeredon":
                orderBy = "registeredOn";
                break;

            default:
                orderBy = "firstName";
                break;
        }
        return userService.getAllUsers(page, size, orderBy, order);
    }

    @GetMapping("{id}")
    ProfileResponse getProfile(@PathVariable int id) {
        logger.info("getProfile({}) called to get profile of another user", id);

        return userService.getUserProfile(id);
    }

    @GetMapping("me")
    ProfileResponse getProfile() {
        logger.info("getProfile() called to get profile for authenticated user");

        return userService.getCurrentUserProfile();
    }

    @GetMapping("emailverification/{vid}")
    ResponseEntity emailVerification(@PathVariable int vid, @RequestParam String seed, HttpServletRequest request) {
        logger.info("emailVerification({}, {}, request) called", vid, seed);

        return userService.verifyEmailActivation(vid, seed, request)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }

    @GetMapping("{id}/image")
    ImageResponse getImage(@PathVariable int id) {
        logger.info("getImage({}) called", id);

        return userService.getImage(id);
    }

    @PostMapping
    ResponseEntity registerUser(@Valid @RequestBody UserCreateRequest request,
                                BindingResult bindingResult) {
        //TODO: laat maar x aantal inserts toe per IP per x tijd
        logger.info("registerUser(...) called");

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        return ResponseEntity.ok(userService.createUser(request));
    }

    @PutMapping("{id}")
    ResponseEntity editUser(@PathVariable int id,
                            @Valid @RequestBody UserEditRequest request,
                            BindingResult bindingResult) {
        logger.info("editUser({}) called", id);

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        return ResponseEntity.ok(userService.editUser(id, request));
    }

    @PutMapping("{id}/image")
    void setImage(@PathVariable int id, MultipartFile image) throws Exception {
        logger.info("setImage({}) called", id);

        Set<String> imageContentTypes = new HashSet<>();
        imageContentTypes.add("image/png");
        imageContentTypes.add("image/jpeg");

        if (image == null || image.getContentType() == null ) {
            logger.info("No image uploaded because image of content type was null");
            throw new BadRequestException("Er was geen afbeelding geüpload");
        } else {
            if (imageContentTypes.parallelStream().anyMatch(image.getContentType()::contains)) {
                userService.setImage(id, image);
                logger.info("Image of content type {} uploaded", image.getContentType());
            } else {
                logger.info("Content type of uploaded image isn't supported: {}", image.getContentType());
            }
        }
    }

    @DeleteMapping("{id}/image")
    void deleteImage(@PathVariable int id) {
        logger.info("deleteImage({}) called", id);
        userService.deleteImage(id);
    }

    @DeleteMapping("{id}")
    void deleteUser(@PathVariable int id) {
        logger.info("deleteUser({}) called", id);
        userService.deleteUser(id);
    }
}
