/*
 * Copyright (c) 2018-2019 Isaak Malik
 *
 * This software is released under the open source EUPL v1.2 license
 * See the LICENSE file or get a copy at: https://eupl.eu/1.2/en
 *
 * Deze broncode is vrijgegeven onder de open source licentie EUPL v1.2
 * Zie het LICENTIE bestand of bezoek: https://eupl.eu/1.2/nl
 */

package be.ambrassade.jeugdlink.controller;

import be.ambrassade.jeugdlink.exceptions.BadRequestException;
import be.ambrassade.jeugdlink.model.request.QuestionRequest;
import be.ambrassade.jeugdlink.model.response.QuestionResponse;
import be.ambrassade.jeugdlink.service.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }


    @GetMapping
    Page<QuestionResponse> getAllQuestions(@RequestParam(required = false, defaultValue = "0") int page,
                                           @RequestParam(required = false, defaultValue = "20") int size,
                                           @RequestParam(required = false, defaultValue = "new") String filter) {
        logger.info("getAllQuestions({}, {}, {}) called", page, size, filter);
        return questionService.getAllQuestions(page, size, filter, null);
    }


    @GetMapping("{id}")
    QuestionResponse getQuestion(@PathVariable int id) {
        logger.info("getQuestion({}) called", id);
        return questionService.getQuestionAndRaiseViewsByOne(id);
    }


    @GetMapping(value = "/search", params = {"question"})
    ResponseEntity searchQuestions(@RequestParam String question,
                                   @RequestParam(required = false, defaultValue = "0") int page,
                                   @RequestParam(required = false, defaultValue = "20") int size) {
        logger.info("searchQuestions(question...) called");

        if (question.length() < 3 || question.length() > 255) {
            logger.warn("There was a validation error!");
            throw new BadRequestException("Search term must be > 3 and w 255");
        }
        return ResponseEntity.ok(questionService.searchForQuestions(question, page, size));
    }

    @GetMapping(value = "/search", params = {"tag"})
    ResponseEntity searchQuestions(@RequestParam int tag,
                                   @RequestParam(required = false, defaultValue = "none") String filter,
                                   @RequestParam(required = false, defaultValue = "0") int page,
                                   @RequestParam(required = false, defaultValue = "20") int size) {
        logger.info("searchQuestions(tag...) called");
        logger.info("Filter search results by {}", filter);

        Page<QuestionResponse> questions;

        switch (filter) {
            case "answered":
                questions = questionService.searchForAnsweredQuestionsByTagId(tag, page, size);
                break;

            case "popular":
                questions = questionService.searchForPopularQuestionsByTagId(tag, page, size);
                break;

            default:
                questions = questionService.searchForQuestionsByTagId(tag, page, size);
                break;
        }
        return ResponseEntity.ok(questions);
    }


    @PostMapping
    ResponseEntity askQuestion(@Valid @RequestBody QuestionRequest request, BindingResult bindingResult) {
        logger.info("askQuestion() called");

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        return ResponseEntity.ok(questionService.askQuestion(request));
    }


    @PutMapping("{id}")
    ResponseEntity editQuestion(@PathVariable int id, @Valid @RequestBody QuestionRequest request, BindingResult bindingResult) {
        logger.info("editQuestion({}) called", id);

        if (bindingResult.hasErrors()) {
            logger.warn("There were {} validation errors!", bindingResult.getAllErrors().size());
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }
        questionService.editQuestion(id, request);
        return ResponseEntity.ok().build();
    }


    @DeleteMapping("{id}")
    void deleteQuestionAndAnswers(@PathVariable int id) {
        logger.info("deleteQuestionAndAnswers({}) called", id);
        questionService.deleteQuestion(id);
    }
}
